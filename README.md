# Pandos+ - lso22az02

### Due parole riguardo questo "gruppo" e lo stato del progetto

Consegno questo progetto in solitaria.

A Settembre 2021 `lso22az02` comprendeva quattro persone. A Dicembre 2021
eravamo gia' in due. Il mio collega rimasto, dopo aver partecipato
'marginalmente' alle prime due fasi, mi promise dopo la consegna di Fase 2 una
piu' efficace partecipazione a Settembre; ho quindi accettato di aspettarlo in
modo da lavorare insieme a Fase 3.
Da fine Agosto l'ho ripetutamente contattato senza piu' ricevere risposta.

Piu' che il carico di lavoro aggiuntivo, mi e' pesato il 'poco' tempo risultante
da questa situazione: con altro tempo (o altre teste) mi sarebbe piaciuto dare
ulteriormente la caccia ad un bug che e' rimasto anche nella versione attuale.

Per il resto il progetto e' conforme alle specifiche, comprese le ottimizzazioni
suggerite al capitolo 4.10.

### Il bug in questione - or, il mio Moby-Dick personale

A volte, immettendo nel terminale le stringhe necessarie al completamento del
test strConcat.c, parte una probabile race condition, che risulta in un kernel
panic, se son presenti anche altri processi, o in un loop infinito di interrupt da
parte dell'IT se il processo e' l'ultimo non bloccato.

Il problema e' anche solo riprodurre il bug, perche' si presenta abbastanza raramente
e non sembra dipendere dall'input in se'.

Sospetto tra l'altro che potrebbe essere una 'finger race condition': ho notato che
scrivendo lentamente non si presenta mai, mentre e' piu' probabile causarlo scrivendo
in fretta. Dovrei indagare piu' a fondo su come l'emulatore gestisce il terminale
per capire se e' una strada possibile.

Non credo che ci sia un errore nel codice di Fase 3, il codice che gestisce la lettura
da terminale e' abbastanza lineare. Piu' probabilmente c'e' un'errore nella gestione
degli interrupt scatenati dalla scrittura sul terminale.

### About la velocita' di stampa

Dopo la scorsa consegna ci era stato lasciato un appunto sulla velocita' di stampa
nel terminale.

Ho riguardato il codice della DOIO e riscritto il codice che si occupava di
recuperare il semaforo relativo ad un certo indirizzo di registro di comando.
Quella funzione eseguiva una marea di cicli e ad occhio era la piu' costosa;
ora e' molto piu' efficiente. La stampa nei terminali attualmente mi sembra
veloce, anche se non ho numeri precisi con cui confrontare le mie prestazioni,
quindi non posso avere la certezza di aver risolto il problema!

## Ottenere il codice

Il file prodotto da `make tar` verra' consegnato come da accordi presi
sulle macchine del laboratorio.

Il codice sorgente del progetto si puo' anche trovare su [GitLab](https://gitlab.com/CarloDePieri/so2122).

## Compilazione ed esecuzione del progetto

Il [makefile](./Makefile) contiene due target principali:

- `main` compilera' il progetto con l'entrypoint contenuto nel file
 [src/main.c](./src/main.c) (il process initiator si trova invece
 dentro [src/init\_proc.c](./src/init_proc.c))

- `run` invece eseguira' l'emulatore, caricando la configurazione `umps3.json`

L'intero codice sorgente e' attentamente commentato e documentato (vedere sotto
come compilare le docs con doxygen).

## FASE 3

Particolari decisioni sono documentate direttamente nel codice.

### Divisione moduli

- [init\_proc.c](./src/init_proc.c): contiene la funzione test che lancia i vari
 tester e l'inizializzazione delle strutture di supporto

- [vm\_support.c](./src/vm_support.c): gestisce gli aspetti relativi alla memoria virtuale

- [sys\_support.c](./src/sys_support.c): contiene gli exception e program trap
 handlers e il syscall manager

- [devices.c](./src/devices.c): si occupa dell'interazione con i device (flash, printer, terminali)

## FASE 2

### Divisione moduli

- [scheduler.c](./src/scheduler.c): contiene la funzione di scheduling principale

- [exceptions.c](./src/exceptions.c): contiene una funzione di gestione delle
eccezioni e alcune funzioni di libreria che vengono utilizzate anche da altri
moduli (kill, verhogen, passeren, pass\_up\_or\_die)

- [interrupt\_manager.c](./src/interrupt_manager.c): gestisce gli interrupt dei
device e dei timer

- [syscall\_manager.c](./src/syscall_manager.c): gestisce gli interrupt sulla
linea 8: le syscall

- [time\_manager.c](./src/time_manager.c): una collezione di funzioni correlate
al mantenimento dei timer e dell'assegnazione del tempo di esecuzione ai processi

### pcb.c, asl.c e relativi headers

Questi moduli contengono rispettivamente:

- le strutture dati e le funzioni relative alle liste e agli alberi di process
control blocks

- le strutture dati e le funzioni relative alla gestione dei semafori

### myutils.c e myutils.h

Contengono una serie di funzioni e macro di libreria che abbiamo implementato
per semplificarci il debugging del codice. Tra le altre cose:

- un list\_len

- memset e memcpy

- una print con overloading (sia in numero di argomenti che in tipo) che stampi
sia interi, che indirizzi, che numeri in binario

- una versione della print di cui sopra che logga in un buffer circolare in memoria

- TRACE: una macro per tracciare facilmente l'area di memoria di un oggetto generico

### Note implementative

Il campo in cui abbiamo applicato piu' discrezionalita' e' stato la gestione
del tempo. Nello specifico:

- Ai current process viene assegnato il tempo necessario a gestire un'eccezione,
tranne nel caso della linea 2, poiche' il mantenimento del IT ci sembrava
qualcosa di cui si dovesse prendere carico il kernel

- Quando si gestisce l'interrupt di un device, il tempo di gestione viene assegnato
al processo che viene sbloccato (poiche' era stato lui ad utilizzare quel device)

- Se ad un processo rimane parte di un time slice, si blocca su un semaforo e
poi viene sbloccato (e quindi restituito alle code ready) non riprende il time slice
da dove lo aveva interrotto, ma lo scheduler gli riassegnera' un nuovo time slice
intero. Abbiamo fatto questa scelta perche' trovavamo strano far eseguire un
processo per magari meno di un ms dopo esser stato bloccato per magari tanto tempo.

- Poiche' le nostre funzioni di logging possono potenzialmente richiedere anche
diversi ms per completare, abbiamo deciso di non assegnare questo tempo ai
processi quando le log sono nel codice del kernel (anche il PLT lo ignora)

- Il mantenimento del IT viene fatto tramite timestamp, in modo da non accumulare
ritardo nel tempo

Un altro punto su cui abbiamo voluto prendere una decisione riguarda il
comportamento della syscall TERMPROCESS nel caso in cui venga richiesta su un
pid a cui non corrisponde nessun processo (perche' magari e' stato gia' ucciso):
la nostra SYSCALL(TERMPROCESS, ..) ritorna 0, nel caso in cui l'operazione abbia
avuto successo, 1 altrimenti.
Poiche' questo caso non era contemplato dal p2test, abbiamo scritto il test relativo.

## Documentazione

Lanciare `make docs` compilera' con doxygen nella cartella `docs/` la
documentazione. La versione html e' facilmente ricercabile, mentre quella
latex puo' produrre un pdf (lanciando `make` da dentro la relativa cartella).

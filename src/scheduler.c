#include <umps/libumps.h>

#include <pcb.h>
#include <asl.h>
#include <myutils.h>
#include <time_manager.h>
#include <main.h>


/**
 * The process scheduler.
 **/
void schedule() {

  while (TRUE) {

    char *logger = "schd";

    if (emptyProcQ(&hp_ready_queue)) {

      // Choose from LOW priority queue
      if (emptyProcQ(&lp_ready_queue)) {

        if (process_count <= 0) {
          // No more processes, shut down the emulator
          hlogl("No more process. HALT");
          HALT();
        }
        
        if (soft_block_count > 0) {

          hlogl("Soft-blocked processes detected. WAIT");
          // enable interrupts
          setSTATUS((getSTATUS() | IECBITON | IMON ));
          // put off PLT
          setTIMER(0xFFFFFFFF);

          WAIT();
        }
        else {
          hlogl("Deadlock detected! PANIC");
          // DEADLOCK
          PANIC();
        }
      }
      else {

        hlogl("Start LP process with pid %i", headProcQ(&lp_ready_queue)->p_pid);
        current_process = removeProcQ(&lp_ready_queue);

        TM_schedule_start_lp();

        LDST((void *) &current_process->p_s);
      }
    }
    else  {
      // Choose from HIGH priority queue
      
      hlogl("Start HP process with pid %i", headProcQ(&hp_ready_queue)->p_pid);
      current_process = removeProcQ(&hp_ready_queue);

      TM_schedule_start_hp();

      LDST((void *) &current_process->p_s);
    }
  }
}

#include <umps/libumps.h>
#include <umps/arch.h>
#include <umps/cp0.h>

#include <interrupt_manager.h>
#include <myutils.h>
#include <exceptions.h>
#include <scheduler.h>
#include <asl.h>
#include <time_manager.h>
#include <main.h>


/**
 * Handles the device and timer interrupts exceptions.
 *
 * @param saved_state The state before the exception, saved in the bios data page
 **/
void interrupt_handler(state_t *saved_state) {

  char *logger = "intr";

  // find out the interrupt line
  int line = 0;
  for (; line < 8; line++) {
    if ((saved_state->cause & CAUSE_IP(line)) != 0) {
      break;
    }
  }

  hlogl("Line %i", line);

  if ( line == 1 ) {
    // Processor Local Timer - PLT
    setTIMER(0xFFFFFFFF);

    // it can happen that the PLT triggers before we can shut it down during an exception management. if during that
    // exception management the current process is no more in a 'running' state (killed or blocked), this PLT exception
    // can get here without a current proccess. hence this control:
    if (current_process != NULL) {
      current_process->p_s = *saved_state;
      returnPcbToQueue(current_process);
      TM_bill_before_schedule(current_process);
      current_process = NULL;
    }

    schedule();
  }
  else if ( line == 2 ) {
    // Interval Timer - Global timer

    set_interval_timer();

    // unblock ALL pcb on the pseudo-clock sem
    int *sem_key = &device_semaphores[48];
    pcb_t *unblocked = NULL; // unused here
    int rc;
    do {
      rc = verhogen(sem_key, NULL, &unblocked);
    }
    while(rc == 1);

    // make sure this semaphore value stays 0
    device_semaphores[48] = 0;

    // the time spent during this exception should not be billed to the current process
    TM_continue(saved_state);
  }
  else if ( 3 <= line && line  <= 7)  {
    // Non-timer device
    devregarea_t *deviceRegs = (devregarea_t *) RAMBASEADDR;
    unsigned int device_bitmap =  deviceRegs->interrupt_dev[line - 3];

    int device = 0;
    for (; device < 8; device++) {
      if ((device_bitmap & 0x1) == 1) {
        break;
      }
      device_bitmap = device_bitmap >> 1;
    }

    memaddr devAddrBase = DEV_REG_ADDR(line, device);

    int *sem_key;
    unsigned int status;


    if (line != 7) {
      // non-timer, non-terminal device
      dtpreg_t *device_struct = (dtpreg_t *) devAddrBase;
      
      // save the status code
      status = device_struct->status;

      // ACK
      device_struct->command = 0x1;

      // Recover the semaphore key
      sem_key = _findDeviceSemKey(line, device);
    }
    else {
      // terminal device
      termreg_t *term_struct = (termreg_t *) devAddrBase;

      int mode;
      // 1 device ready
      // 3 device busy
      if ((term_struct->recv_status & 0xFF) != 1 && (term_struct->recv_status & 0xFF) != 3) {
        // the recv sub device raised an interrupt
        mode = 0;
        status = term_struct->recv_status;
        term_struct->recv_command = 0x1;
      }
      else {
        // the transm sub device raised an interrupt
        mode = 1;
        status = term_struct->transm_status;
        term_struct->transm_command = 0x1;
      }

      // Recover the semaphore key
      sem_key = _findTerminalSemKey(device, mode);
    }

    // Perform a V operation on the device semaphore and return the pcb to the right process queue
    pcb_t *unblocked_pcb = NULL;
    verhogen(sem_key, NULL, &unblocked_pcb);

    // could be NULL if the process who caused the interrupt was killed
    if (unblocked_pcb != NULL) {
      // save the device status code to the pcb v0 register
      unblocked_pcb->p_s.reg_v0 = status;

      // decrement soft_block_count
      soft_block_count--;
    }

    // bill the unblocked process and continue the timeslice of the current process, if present
    TM_bill_and_continue(unblocked_pcb, current_process, saved_state);
  }

}

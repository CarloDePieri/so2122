#include <myutils.h>
#include <vm_support.h>
#include <sys_support.h>
#include <devices.h>
#include <init_proc.h>


//-----------------------------------------------------------------------------------------------------
//
//    SUPPORT SEMAPHORES
//
//    NOTE 
//    The support_mutex struct and the relative functions below are used to allow the support level 
//    to easly keep track of which process locked a resource (like a device or the swap pool table).
//    This is usefull to release those resources if the process die.
//
//-----------------------------------------------------------------------------------------------------


// semaphore used to kill the test process gracefully
int masterSemaphore = 0;

/**
 * Perform a V operation on the semaphore used to coordinate test processes.
 **/
void masterSemaphore_V () {
  SYSCALL(VERHOGEN, (int)&masterSemaphore, 0, 0);
}

/**
 * Perform a P operation on the semaphore used to coordinate test processes.
 **/
void masterSemaphore_P () {
  SYSCALL(PASSEREN, (int)&masterSemaphore, 0, 0);
}

/**
 * A lock operation on a support level mutex by the process with the given asid.
 *
 * @param asid The asid of the process
 * @param mutex A support mutex structure pointer
 **/
void support_mutex_lock(int asid, support_mutex *mutex) {
  // Do this atomically, so that the asid information can't be lost
  IOFF();
  SYSCALL(PASSEREN, (int)&mutex->sem, 0, 0);
  // only assign the asid after the lock has been acquired
  mutex->asid = asid;
  ION();
}

/**
 * An unlock operation on a support level mutex by the process with the given asid.
 *
 * @param asid The asid of the process
 * @param mutex A support mutex structure pointer
 **/
void support_mutex_release(int asid, support_mutex *mutex) {
  // Do this atomically, so that when the asid is reset the resource is obbligatory released
  IOFF();
  mutex->asid = -1;
  SYSCALL(VERHOGEN, (int)&mutex->sem, 0, 0);
  ION();
}

/**
 * An unlock operation on a support level mutex by the process with the given asid. This is ONLY performed
 * if the specified asid is linked to that mutex. 
 *
 * @param asid The asid of the process that holds the resource
 * @param mutex A support mutex structure pointer
 **/
void support_mutex_release_if_held_by_asid(int asid, support_mutex *sem) {
  if (asid == sem->asid) {
    support_mutex_release(asid, sem);
  }
}


//--------------------------------------------------------------------------------------
//
//    SUPPORT STRUCTURES
//
//    NOTE Here it's implemented the allocate/deallocate paradigm suggested by 4.10.
//
//--------------------------------------------------------------------------------------


static support_t support_table[UPROCMAX];
struct list_head available_support_t;

/**
 * Return the next free support structure.
 *
 * @return A free support structure
 **/
support_t *allocate() {
  if(list_empty(&available_support_t)) {
    char *logger = "test";
    hlogl("No available support structure left!");
    PANIC();
  }
  // pop the next free availale structure from the list
  struct list_head *next_lh = list_next(&available_support_t);
  list_del(next_lh);
  // return the support_t
  return container_of(next_lh, support_t, p_list);
}

/**
 * Put back the given support structure in the free support strcuture list.
 *
 * @param support The support structure that must be returned to the free list
 **/
void deallocate(support_t *support) {
  list_add(&support->p_list, &available_support_t);
}

/**
 * Init the free support structure list, preparing UPROCMAX structures.
 **/
void init_support_structures() {
  INIT_LIST_HEAD(&available_support_t);
  for (int i = 0; i < UPROCMAX; i++) {
    deallocate(&support_table[i]);
  }
}


//---------------------------
//
//       INSTANTIATION
//
//---------------------------


/**
 * The instantiatior process. It will launch tester processes and wait for them to finish.
 **/
void test() {

  // init all needed structures
  init_swap_pool();
  init_devices_mutex();
  init_support_structures();

  // prepare the pcb state
  state_t p_state;
  STST(&p_state);
  // set stack and program pointers
  p_state.reg_sp = (memaddr)USERSTACKTOP;
  p_state.pc_epc = p_state.reg_t9 = (memaddr)UPROCSTARTADDR; 
  // the status that tester processes will have: all interrupts enabled, processor local timer enabled and user-mode
  p_state.status = p_state.status | IEPON | IMON | TEBITON | USERPON;

  // the status that exception handlers will have: all interrupts enabled, processor local timer enabled 
  // and kernel-mode
  static unsigned int support_status = 0 | IEPON | IMON | TEBITON;

  // reserve space for this function stack at ramtop
  memaddr ramtop;
  RAMTOP(ramtop);
  memaddr stacktop = ramtop - 2 * PAGESIZE;

  for (int asid = 1; asid <= UPROCMAX; asid++) {

    // calculate the correct stack pages
    memaddr stackPtr = stacktop - (asid - 1) * 2 * PAGESIZE;

    // write this proess asid into entryHI
    p_state.entry_hi = asid << ASIDSHIFT;

    // recover a free support structure
    support_t *support = allocate();

    // prepare the process user page table
    init_private_page_table(support->sup_privatePgTbl, asid);

    // prepare the support structure with both exception handlers context
    support->sup_asid = asid;

    support->sup_exceptContext[PGFAULTEXCEPT].pc = (memaddr)support_tlb;
    support->sup_exceptContext[PGFAULTEXCEPT].stackPtr = stackPtr;
    support->sup_exceptContext[PGFAULTEXCEPT].status = support_status;

    support->sup_exceptContext[GENERALEXCEPT].pc = (memaddr)support_generic;
    support->sup_exceptContext[GENERALEXCEPT].stackPtr = stackPtr - PAGESIZE;
    support->sup_exceptContext[GENERALEXCEPT].status = support_status;

    // launch the process!
    SYSCALL(CREATEPROCESS, (int)&p_state, PROCESS_PRIO_LOW, (int)support);

  }

  // wait for all children to terminate before dying 
  for (int i = 1; i <= UPROCMAX; i++) {
    masterSemaphore_P();
  }

  // So long and thanks for all the fish!
  SYSCALL(TERMPROCESS, 0, 0, 0);
}

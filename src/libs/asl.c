/**
 * asl.c
 *
 * This module handles the semaphores lists and all related functions.
 *
 **/
#include <pandos_types.h>
#include <pcb.h>
#include <asl.h>
#include <myutils.h>


//-------------------
//  DATA STRUCTURES
//-------------------


// The semaphores table
static struct semd_t semd_table[MAXPROC];
// The FREE semaphores list
LIST_HEAD(semdFree_h);
// The ACTIVE semaphores list (ASL)
LIST_HEAD(semd_h);


//-------------------
//     HELPERS
//-------------------


/**
 * Move a semaphore from the free semaphores list to the ASL.
 *
 * @param semAdd The pointer key the new semaphore will use
 *
 * @return the pointer to the new semaphore if one was available, NULL otherwise
 **/
semd_t *_allocSemd(int *semAdd) {

  if (!list_empty(&semdFree_h)) {
    // remove from the free semaphores list
    struct list_head *sem_list_head = list_next(&semdFree_h);
    list_del(sem_list_head);

    // add it to the ASL
    list_add(sem_list_head, &semd_h);

    // Set the semaphore's s_key
    semd_t *sem = container_of(sem_list_head, semd_t, s_link);
    sem->s_key = semAdd;

    // init the semaphore's process queue
    mkEmptyProcQ(&sem->s_procq);

    // return the semaphore
    return sem;
  }
  else
    return NULL;
}


/**
 * Block the pcb on a semaphore.
 *
 * @param p The pcb pointer
 * @param sem The active semaphore pointer
 **/
void _blockPcb(struct pcb_t *p, struct semd_t *sem) {
  p->p_semAdd = sem->s_key;
  list_add_tail(&p->p_list, &sem->s_procq);
}


/**
 * Remove the pcb from the semaphore pcb list; if the semaphore pcb list is empty, move it to the free semaphores list.
 *
 * @param p The pcb pointer
 * @param sem The semaphore pointer
 *
 * @return the unblocked pcb pointer
 **/
pcb_t *_unblockPcb(pcb_t *p, semd_t * sem) {

  // unlink the pcb from the sem and reset the p_semAdd key to the default value
  list_del(&p->p_list);
  p->p_semAdd = NOT_A_SEM_KEY;

  if (list_empty(&sem->s_procq)) {
    // the sem has no more pcb blocked on it, unlink it from the ASL and put it back in the free sem list
    list_del(&sem->s_link);
    list_add(&sem->s_link, &semdFree_h);
  }

  return p;
}


//-----------------------
// MODULE MAIN FUNCTIONS
//-----------------------


/**
 * Initialize the free semaphores list to hold MAXPROC semaphores.
 **/
void initASL() {
  for (int i=0; i<MAXPROC; i++) {
    list_add(&semd_table[i].s_link, &semdFree_h);
  }
}


/**
 * Block the pcb on a semaphore with the given key.
 *
 * @param semAdd The pointer key of the semaphore
 * @param p The pcb pointer
 *
 * @return FALSE if the pcb was successfully blocked, TRUE otherwise
 **/
int insertBlocked(int *semAdd, pcb_t *p) {

  // look for a semaphore with the given key in the ASL 
  struct list_head* iter;
  list_for_each(iter,&semd_h) {
    semd_t *sem = container_of(iter, semd_t, s_link);
    if (sem->s_key == semAdd) {
      // semaphore with that key found, block the pcb on the semaphore
      _blockPcb(p, sem);
      return FALSE;
    }
  }

  // if we got here it means that no sem with the given key was in the ASL

  // try to allocate a new sem
  semd_t *sem = _allocSemd(semAdd);

  if (sem != NULL) {
    // new semaphore found, block the pcb on the semaphore
    _blockPcb(p, sem);
    return FALSE;
  }
  else
    return TRUE;
}


/**
 * Unblock the first pcb blocked on the semaphore with the given key. If the semaphore is emptied, move it to the free
 * semaphores list.
 *
 * @param semAdd The pointer key of the semaphore
 *
 * @return the unblocked pcb pointer or NULL if the specified semaphore was not active
 **/
pcb_t *removeBlocked(int *semAdd) {
  
  // look for a semaphore with the given key in the ASL 
  struct list_head* iter;
  list_for_each(iter,&semd_h) {

    semd_t *sem = container_of(iter, semd_t, s_link);
    if (sem->s_key == semAdd) {

      // semaphore found, so it must have at least a pcb blocked. Retrieve it
      struct list_head *first_pcb_lh = list_next(&sem->s_procq);
      pcb_t *p = container_of(first_pcb_lh, pcb_t, p_list);

      // unblock that pcb and return it
      return _unblockPcb(p, sem);
    }
  }

  // there are no active sem with that semAdd
  return NULL;
}


/**
 * Unblock the pcb from the semaphore where it's blocked on. If the semaphore is emptied, move it to the free
 * semaphores list.
 *
 * @param p The pcb pointer
 *
 * @return the unblocked pcb pointer or NULL if the pcb was not actually blocked
 **/
pcb_t *outBlocked(pcb_t *p) {

  // look for a semaphore with the key equal to p->p_semAdd in the ASL 
  struct list_head* iter;
  list_for_each(iter,&semd_h) {

    semd_t *sem = container_of(iter, semd_t, s_link);
    if (sem->s_key == p->p_semAdd) {
      // semaphore found, unblock the pcb (and release the semaphore if needed)
      return _unblockPcb(p, sem);
    }
  }

  // ERROR: could not find an active semaphore with the provided key
  return NULL;
}


/**
 * Return the first pcb blocked on a semaphore, without unblocking it.
 *
 * @param semAdd The semaphore pointer key
 *
 * @return the blocked pcb pointer or NULL if no active semaphore with that key was found
 **/
pcb_t *headBlocked(int *semAdd){

  // look for a semaphore with the given key in the ASL 
  struct list_head* iter;
  list_for_each(iter,&semd_h) {

    semd_t *sem = container_of(iter, semd_t, s_link);
    if (sem->s_key == semAdd) {
      // active sem found, return the first blocked pcb
      struct list_head *first_pcb_lh = list_next(&sem->s_procq);
      return container_of(first_pcb_lh, pcb_t, p_list);
    }
  }

  // there are no active sem with that semAdd
  return NULL;
}

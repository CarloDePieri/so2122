/**
 * pcb.c
 *
 * This module handles the process control blocks lists and trees and all related functions.
 *
 **/
#include "listx.h"
#include <pandos_types.h>
#include <myutils.h>
#include <asl.h>


//-------------------
//  DATA STRUCTURES
//-------------------


// The pcb table
static struct pcb_t pcbFree_table[MAXPROC];
// The free pcb list
LIST_HEAD(pcbFree_h);
// process id
extern int last_pid;

//---------------------
//      PCB LISTS
//---------------------


/**
 * Init the free pcb list with every pcb contained in the pcb table.
 **/
void initPcbs() {
  for (int i=0; i<MAXPROC; i++) {
    list_add(&pcbFree_table[i].p_list, &pcbFree_h);
  }
}


/**
 * Initialize the list pointed by `head` as an empty list.
 *
 * @param head The sentinel pointing to the to-be empty list
 **/
void mkEmptyProcQ(struct list_head *head) {
  INIT_LIST_HEAD(head);
}


/**
 * Initilize and return a pcb if there's one available in the free pcb list.
 *
 * @return the pcb pointer, NULL otherwise
 **/
pcb_t *allocPcb() {

  if (!list_empty(&pcbFree_h)) {

    // unlink the pcb from the free pcb list
    struct list_head *head = list_next(&pcbFree_h);
    list_del(head);

    // recover the actual pcb
    struct pcb_t *head_pcb = container_of(head, pcb_t, p_list);

    // set all the pcb memory to 0, making sure it's ready to be reused
    memset(head_pcb, 0, sizeof(struct pcb_t));

    // set the pcb sem pointer to the default one
    head_pcb->p_semAdd = NOT_A_SEM_KEY;

    // initialize all lists to empty list
    mkEmptyProcQ(&head_pcb->p_list);
    mkEmptyProcQ(&head_pcb->p_child);
    mkEmptyProcQ(&head_pcb->p_sib);

    // set the processor time to 0
    head_pcb->p_time = 0;

    // set the support structure pointer to NULL
    head_pcb->p_supportStruct = NULL;

    // by default, is low priority
    head_pcb->p_prio = 0;

    // set the process id an ever growing integer
    head_pcb->p_pid = ++last_pid;

    return head_pcb;
  }
  else
    return NULL;
}


/**
 * Add the pointed pcb instance to the free pcb list.
 *
 * @param p The pcb pointer
 **/
void freePcb(pcb_t *p) {
  list_add(&(p->p_list), &pcbFree_h);
}


/**
 * Check if a list is empty.
 *
 * @param head The list sentinel
 *
 * @return TRUE if the list is empty, FALSE otherwise
 **/
int emptyProcQ(struct list_head *head) {
  return list_empty(head);
}


/**
 * Enqueue the pcb pointed by `p` into the list pointed by `head`.
 *
 * @param head The list sentinel
 * @param p The pcb pointer
 **/
void insertProcQ(struct list_head *head, pcb_t *p) {
  list_add_tail(&p->p_list, head);
}


/**
 * Return the element at the head of the list pointed by `head`, without removing it from the list.
 *
 * @param head The list sentinel
 *
 * @return the pointer to the first object of the list or NULL if the list is empty
 **/
pcb_t *headProcQ(struct list_head *head) { 
  if (!list_empty(head)) {
    // the list is not empty, recover the first object and return it
    struct list_head *next = head->next;
    return container_of(next, pcb_t, p_list);
  }
  else {
    return NULL;
  }
}


/**
 * Remove the first object from the list pointed by `head` and return it.
 *
 * @param head The list sentinel
 *
 * @return the pointer to the removed object or NULL if the list is empty
 **/
pcb_t *removeProcQ(struct list_head *head) {
  if (!list_empty(head)) {
    // the list is not empty, recover the first object
    struct list_head *next = head->next;
    struct pcb_t *head_pcb = container_of(next, pcb_t, p_list);
    // remove the object from the list
    list_del(next);
    return head_pcb;
  }
  else {
    return NULL;
  }
}


/**
 * Remove the pcb pointed by `p` from the list pointed by `head`. 
 *
 * @param head The list sentinel
 * @param p The pcb pointer
 *
 * @return the removed pcb if it was found in the list, NULL otherwise
 **/
pcb_t *outProcQ(struct list_head *head, pcb_t *p) {
  if (!list_empty(head)) {
    // look for the pcb in the list
    struct list_head* iter;
    list_for_each(iter,head) {
      if (iter == &p->p_list) {
        // found the pcb, remove it from the list
        list_del(&p->p_list);
        return p;
      }
    }
    // if we got here the pcb was not found in the given list
    return NULL;
  }
  else {
    return NULL;
  }
}


//---------------------
//      PCB TREES
//---------------------


/**
 * Check if the pcb pointed by `p` has children.
 *
 * @param p The pcb pointer
 *
 * @return TRUE if the pcb has no children, FALSE otherwise
 **/
int emptyChild(pcb_t *p) {
  return list_empty(&p->p_child);
}


/**
 * Insert the pcb pointed by `p` into the children list of the pcb pointed by `prnt`.
 *
 * @param p The child pcb pointer
 * @param prnt The parent pcb pointer
 **/
void insertChild(pcb_t *prnt, pcb_t *p) {
  list_add(&p->p_sib, &prnt->p_child);
  p->p_parent = prnt;
}


/**
 * Remove the first child pcb from the pointed pcb `p` children list.
 *
 * @param p The parent pcb pointer
 *
 * @return the first child pcb pointer or NULL if the parent has no children
 **/
pcb_t *removeChild(pcb_t *p) {

  if (!list_empty(&p->p_child)) {

    // the parent has children, now recover the first one
    struct list_head *child_head = list_next(&p->p_child);
    struct pcb_t *child = container_of(child_head, pcb_t, p_sib);

    // remove it from the parent's children list
    list_del(child_head);

    // return the removed child
    return child;
  }
  else {
    // the pcb has no children!
    return NULL;
  }

}


/**
 * Remove the pcb pointed by `p` from its parent's children list.
 *
 * @param p The child pcb pointer
 *
 * @return the removed pcb pointer or NULL if the pcb has no parent or is not in its parent's children list
 **/
pcb_t *outChild(pcb_t *p) {
  if (p->p_parent != 0) {
    // the pcb has a parent, now scan its parent's children list
    struct list_head* iter;
    list_for_each(iter,&p->p_parent->p_child) {
      if (iter == &p->p_sib) {
        // found the pcb, remove it from the parent's children list and return it
        list_del(iter);
        return p;
      }
    }
    // ERROR: could not find the pcb in its parent's children list
    return NULL;
  }
  else {
    // the pcb has no parent!
    return NULL;
  }
}

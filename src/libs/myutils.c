#include <pandos_types.h>
#include <klog.h>
#include <myutils.h>
#include <umps/libumps.h>
#include <time_manager.h>

#define TRANSMITTED 5
#define PRINTCHR    2
#define CHAROFFSET  8
#define STATUSMASK  0xFF
#define TERM0ADDR   0x10000254

typedef unsigned int devreg;

extern unsigned int logger_time;


/**
 * Dummy function that can be used as breakpoint in the emulator debugging tool.
 **/
void bp(){};
void bp1(){};
void bp2(){};


//----------------------
//   STRING UTILITIES
//----------------------


/**
 * Compare two strings.
 *
 * @param X The first string
 * @param Y The second string
 *
 * @return 0 if they are equals, the ascii difference otherwise
 **/
int strcmp(const char *X, const char *Y) {
  while (*X) {
    if (*X != *Y) { break; }
    X++;
    Y++;
  }
  return *(const unsigned char*)X - *(const unsigned char*)Y;
}


/**
 * Copy the string `from` in the buffer of `to`.
 *
 * @param to The destination buffer
 * @param from The source buffer
 *
 * @return the pointer to the destination buffer or NULL if no memory was provided
 **/
char *strcpy(char *to, char *from) {
  if (to == NULL)
    return NULL;
  char * ptr = to;
  while(*from != '\0') {
    *to++ = *from++;
  }
  *to = '\0';
  return ptr;
}


//---------------------
//   LIST UTILITIES
//---------------------


/**
 * Calculate the given list length
 *
 * @param head The sentinel of the list
 *
 * @return the list length
 **/
int list_len(struct list_head *head) {
  char ctr = 0;
  if (!list_empty(head)) {
    struct list_head* iter;
    list_for_each(iter,head) {
      ctr++;
    }
  }
  return (int)ctr;
}


//---------------------
//  MEMORY MANAGEMENT
//---------------------


/**
 * A simple memset implementation. Sets `n` bytes to the value `c`, starting from the address `s`.
 *
 * @param s The starting address
 * @param c The value that will be written to the addresses
 * @param n How many bytes will be written
 **/
void* memset(void* s, int c, size_tt n) {
  // since we want to increment byte by byte, treat the pointer as char*
  unsigned char* p = (unsigned char*)s;
  // consider only the first byte of c if its larger than one byte (it shouldn't be)
  unsigned char tmp = c & 0xff;
  while (n--)
    *p++ = tmp;
  return s;
}


/**
 * A simple memcpy implementation. Copies `n` bytes from the `from` address to the `to` address.
 *
 * @param to The source first address 
 * @param from The destination first address
 * @param n How many bytes to copy
 **/
void memcpy(void* to, void* from, size_tt n){
  // since we want to copy byte by byte, treat both pointers as char*
  char *cfrom = (char *)from;
  char *cto = (char *)to;
  // copy byte by byte
  for (int i=0; i<n; i++)
    cto[i] = cfrom[i];
}


//-------------------
//  PRINT UTILITIES
//-------------------
// NOTE: mytermstat and mytermprint are shamelessy taken from p1test. We need them for our main entrypoint.


/**
 * This function returns the terminal transmitter status value given its address.
 *
 * @param stataddr The terminal transmitter address
 *
 * @return the terminal status value
 **/
devreg mytermstat(memaddr *stataddr) {
    return ((*stataddr) & STATUSMASK);
}


/**
 * This function prints a string on specified terminal.
 *
 * @param str The char* that will be printed
 * @param term The terminal id
 *
 * @return TRUE if print was successful, FALSE otherwise
 **/
unsigned int mytermprint(char *str, unsigned int term) {
    memaddr     *statusp;
    memaddr     *commandp;
    devreg       stat;
    devreg       cmd;
    unsigned int error = FALSE;

    if (term < DEVPERINT) {
        /* terminal is correct */
        /* compute device register field addresses */
        statusp  = (devreg *)(TERM0ADDR + (term * DEVREGSIZE) + (TRANSTATUS * DEVREGLEN));
        commandp = (devreg *)(TERM0ADDR + (term * DEVREGSIZE) + (TRANCOMMAND * DEVREGLEN));

        /* test device status */
        stat = mytermstat(statusp);
        if (stat == READY || stat == TRANSMITTED) {
            /* device is available */

            /* print cycle */
            while (*str != EOS && !error) {
                cmd       = (*str << CHAROFFSET) | PRINTCHR;
                *commandp = cmd;

                /* busy waiting */
                stat = mytermstat(statusp);
                while (stat == BUSY)
                    stat = mytermstat(statusp);

                /* end of wait */
                if (stat != TRANSMITTED)
  error = TRUE;
                else
                    /* move to next char */
                    str++;
            }
        } else
            /* device is not available */
            error = TRUE;
    } else
        /* wrong terminal device number */
        error = TRUE;

    return (!error);
}


/**
 * Return the length of the given `string`
 *
 * @param string The string
 *
 * @return the length of the string
 **/
int strlen(char *string) {
  int length = 0;
  for(;string[length]!='\0';length++);
  return length;
}


/** 
 * Copy the `text` string to `new_text`, replacing the first instance of %`placeholder` with `replacement`
 *
 * @param text The text containing the placeholder
 * @param new_text A char array of at least length strlen(text) - 2 + strlen(replacement)
 * @param placeholder A single character, eg 'i'; this would result in '%i' to be replaced in text
 * @param replacement The text that will replace the placeholder
 *
 * @return the pointer to the new buffer
 **/
char *replacePlaceholder(char *text, char *new_text, char placeholder, char *replacement) {

  int old_index = 0;
  int new_index = 0;
  int size = strlen(text);
  int replacement_size = strlen(replacement);
  int found = FALSE;
  
  // iterate over the original text
  for (; old_index <= size; old_index++) {

    if (!found && text[old_index] == '%' && old_index < size - 1 && text[old_index + 1] == placeholder){
      found = TRUE;
      // found the placeholder! now copy the replacement
      for (int replacemente_index = 0; replacemente_index < replacement_size; replacemente_index++) {
        new_text[new_index++] = replacement[replacemente_index];
      }
      old_index++;
    }
    else {
      new_text[new_index++] = text[old_index];
    }
  }

  // correctly terminate the string
  new_text[new_index] = '\0';

  return new_text;
}


#define ADDR_STR_LENGTH 11
// Prepare a new buffer char* from string, ready to be fed as target to replaceAddr
#define ADDR_TARGET_BUFFER(string) char add_target_buffer[strlen(string) - 2 + ADDR_STR_LENGTH]


/**
 * Put in the given `buffer` the string representation of the `ptr` address
 *
 * @param ptr The pointer to get the address from
 * @param buffer The char* that will contain the string address (of size ADDR_STR_LENGTH)
 *
 * @return the pointer to the buffer
 **/
char *addrToChar(memaddr ptr, char *buffer) {
    int y = 0;
    unsigned int p = (unsigned int)ptr;

    buffer[y++] = '0';
    buffer[y++] = 'x';
    for(int i = (sizeof(p) << 3) - 4; i>=0; i -= 4) {
      // for every address byte, get the corresponding char
      int hex_char = (p >> i) & 0xf;
      if (hex_char >= 0 && hex_char < 10) {
        // digits
        buffer[y++] = '0' + hex_char;
      }
      else {
        // letters
        buffer[y++] = 'a' + hex_char - 10;
      }
    }
    buffer[y] = '\0';
    return buffer;
}


/**
 * Replace %p inside the given `string` with the address of `ptr` and put it inside `target`.
 *
 * @param string The text containing %p
 * @param ptr The pointer to the address that will replace %p
 * @param target The target string buffer
 *
 * @return the pointer to the target buffer
 **/
char *replaceAddr(char *string, memaddr ptr, char *target) {

  // convert the ptr address to a string representation
  char address[ADDR_STR_LENGTH];

  // replace the first instance of %p with the address
  return replacePlaceholder(string, target, 'p', addrToChar(ptr, address));
}


/**
 * Print the the given `term` the string `txt`, replacing %p with the address of `ptr`.
 *
 * @param txt The text to be printed, containing %p
 * @param ptr The pointer to the address that will replace %p
 * @param term The terminal that will show the print
 *
 * @return the mytermprint return code
 **/
int printAddr(char *txt, memaddr ptr, int term) {

  ADDR_TARGET_BUFFER(txt);

  // Print it to the given terminal
  return mytermprint(replaceAddr(txt, ptr, add_target_buffer), term);
}


#define INT_STR_LENGTH 11
// Prepare a new buffer char* from string, ready to be fed as target to replaceInt
#define INT_TARGET_BUFFER(string) char int_target_buffer[strlen(string) - 2 + INT_STR_LENGTH]


/**
 * Put into `buffer` the string representation of the given integer `value`
 *
 * @param value The integer
 * @param buffer The string where the number will be written (of size INT_STR_LENGTH )
 *
 * @return the `buffer` string pointer
 **/
char *itoa(int value, char *buffer) {
  unsigned uvalue;
  const char digits[] = "0123456789abcdefghijklmnopqrstuvwxyz";
  int i, j;
  unsigned remainder;
  char c;

  i = 0;
  j = 0;
  if (value < 0) {              
    buffer[i++] = '-';
    j = 1; // this will allow to skip the minus when reversing the string
    uvalue = (unsigned)-value;
  }
  else {
    uvalue = (unsigned)value;
  }

  do {
    remainder = uvalue % 10;
    buffer[i++] = digits[remainder];
    uvalue = uvalue / 10;
  } while (uvalue != 0);  
  buffer[i] = '\0'; 
  
  // reverse the string
  for (i--; j < i; j++, i--) {
    c = buffer[j];
    buffer[j] = buffer[i];
    buffer[i] = c; 
  }       
  return buffer;
}


/**
 * Replace %i inside the given `string` with the textual `number` and put it inside `target`.
 *
 * @param string The text containing %i
 * @param ptr The pointer to the number
 * @param target The target string buffer
 *
 * @return the pointer to the target buffer
 **/
char *replaceInt(char *string, int number, char *target) {

  char txt_num[INT_STR_LENGTH];

  // replace the first instance of %i with the number
  return replacePlaceholder(string, target, 'i', itoa(number, txt_num));
}


#define BIN_STR_LENGTH 36 // 32 + 3 spaces + \0
// Prepare a new buffer char* from string, ready to be fed as target to replaceBin
#define BIN_TARGET_BUFFER(string) char bin_target_buffer[strlen(string) - 2 + INT_STR_LENGTH]


/**
 * Put into `buffer` the string binary representation of the given integer `value`
 *
 * @param value The integer
 * @param buffer The string where the number will be written (of size BIN_STR_LENGTH)
 *
 * @return the `buffer` string pointer
 **/
char *itobin(int a, char *buffer) {
  char * ptr = buffer + BIN_STR_LENGTH - 2;
  for (int i = BIN_STR_LENGTH - 2; i >= 0; i--) {
    if (i == 26 || i == 17 || i == 8) {
      *ptr-- = ' ';
    }
    else {
      *ptr-- = (a & 1) + '0';
      a >>= 1;
    }
  }
  buffer[BIN_STR_LENGTH - 1] = '\0';
  return buffer;
}


/**
 * Replace %b inside the given `string` with the binary representation of the integer `number` and put it inside `target`.
 *
 * @param string The text containing %b
 * @param ptr The pointer to the number
 * @param target The target string buffer
 *
 * @return the pointer to the target buffer
 **/
char *replaceBin(char *string, int number, char *target) {

  char txt_num[BIN_STR_LENGTH];

  // replace the first instance of %b with the number
  return replacePlaceholder(string, target, 'b', itobin(number, txt_num));
}




char _findPlaceholder(char *string) {
  /* char *index = string; */
  while(*string != '\0') {
    char *next = string + 1;
    if (*string == '%' && *next != '\0' && *next == 'b') {
      return 'b';
    } 
    else if (*string == '%' && *next != '\0' && *next == 'i') {
      return 'i';
    } 
    string++;
  }
  return '\0';
}


/**
 * Print to the given `term` the string `txt`, replacing the first %i with the textual integer `number`, or the first
 * %b with the binary representation of the integer `number`.
 *
 * @param txt The text to be printed, containing %i or %b
 * @param number The number that will replace the placeholder
 * @param term The terminal that will show the print
 *
 * @return the mytermprint return code
 **/
int printInt(char *txt, int number, int term) {

  // look for placeholders
  char placeholder = _findPlaceholder(txt);

  if (placeholder == 'b') {
    BIN_TARGET_BUFFER(txt);
    return mytermprint(replaceBin(txt, number, bin_target_buffer), 0);
  }
  else if (placeholder == 'i') {
    INT_TARGET_BUFFER(txt);
    return mytermprint(replaceInt(txt, number, int_target_buffer), 0);
  }
  else {
    return mytermprint(txt, 0);
  }

  return -1;
}


//-------------------------
//   LOG UTILITIES
//-------------------------


/**
 * Log to the circular log the string `string`, replacing the first %p with the hex address of `addr`.
 *
 * @param string The text to be printed, containing %p
 * @param addr The address that will replace the placeholder
 **/
void logAddr(char *string, memaddr addr) {
  ADDR_TARGET_BUFFER(string);
  klog_print(replaceAddr(string, addr, add_target_buffer));
}


/**
 * Log to the circular log the string `string`, replacing the first %i with the textual integer `number` or the first
 * %b with the binary representation of the integer `number`.
 *
 * @param string The text to be printed, containing %i or %b
 * @param number The number that will replace the placeholder
 **/
void logInt(char *string, int number) {

  // look for placeholders
  char placeholder = _findPlaceholder(string);

  if (placeholder == 'b') {
    BIN_TARGET_BUFFER(string);
    klog_print(replaceBin(string, number, bin_target_buffer));
  }
  else if (placeholder == 'i') {
    INT_TARGET_BUFFER(string);
    klog_print(replaceInt(string, number, int_target_buffer));
  }
  else {
    klog_print(string);
  }

}

// Print a nice, informative header when logging
extern pcb_t * current_process;
int last_logged_pid = 0;
char last_logger[KLOG_LINE_SIZE];
void _log_start(char *logger) {
  take_logger_timestamp();
  int pid = 0;
  if (current_process != NULL) {
    pid = current_process->p_pid;
  }
  char * ptr = logger;
  if ( pid == last_logged_pid  && strcmp(last_logger, logger)==0) {
    // fill in with dots, trying not to clutter the klog when the logger and pid are the same
    if (pid >= 0) {
      do {
        pid /= 10;
        log(".");
      } while (pid != 0);
    }
    else
        log(".");
    log(" | ");
    while (*ptr++ != '\0') {
      log(".");
    }
    log(" | ");
  }
  else {
    // new logger/pid, print the header
    if (pid >= 0) {
      last_logged_pid = pid;
      log("%i", last_logged_pid);
    }
    else
      log("0");
    strcpy(last_logger, logger);
    log(" | "); log(last_logger); log(" | ");
  }
}

void _log_end() {
  update_logger_time();
}


//-------------------------
//   DEBUGGING UTILITIES
//-------------------------


/**
 * Quickly check and print to the terminal if two address are the same.
 *
 * @param a The first address
 * @param b The second address
 *
 **/
void assertSameAddr(void *a, void *b) {
  if (a == b) {
    mytermprint("OK", 0);
  }
  else {
    printAddr("ERR: %p != ", (memaddr) a, 0);
    printAddr("%p", (memaddr) b, 0);
    mytermprint("\n\n", 0);
  }
}

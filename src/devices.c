#include <umps/arch.h>
#include <myutils.h>
#include <devices.h>
#include <sys_support.h>
#include <init_proc.h>


//------------------------------------------------------------------------------------
//
//    DEVICES MUTEX
//
//    NOTE These are used to associate a process (via its asid) to a shared device
//
//------------------------------------------------------------------------------------


static support_mutex flash_mutex[DEVPERINT],
                     printer_mutex[DEVPERINT],
                     terminal_w_mutex[DEVPERINT],
                     terminal_r_mutex[DEVPERINT];


/**
 * Init the device mutex matrix.
 **/
void init_devices_mutex()
{
  for (int i = 0; i < DEVPERINT; ++i) {
    flash_mutex[i].sem = printer_mutex[i].sem = terminal_w_mutex[i].sem = terminal_r_mutex[i].sem = 1;
    flash_mutex[i].asid = printer_mutex[i].asid = terminal_w_mutex[i].asid = terminal_r_mutex[i].asid = -1;
  }
}

/**
 * Release any device mutex held by a process with the given ASID.
 *
 * @param asid The asid of the process
 **/
void release_held_devices_mutex(int asid) {
  for (int i = 0; i < DEVPERINT; ++i) {
    support_mutex_release_if_held_by_asid(asid, &flash_mutex[i]);
    support_mutex_release_if_held_by_asid(asid, &printer_mutex[i]);
    support_mutex_release_if_held_by_asid(asid, &terminal_r_mutex[i]);
    support_mutex_release_if_held_by_asid(asid, &terminal_w_mutex[i]);
  }
}


//-------------------------
//
//        UTILITIES
//
//-------------------------


/**
 * Check if the provided string size is valid.
 *
 * @param length The string length
 *
 * @return 1 if it's valid, 0 otherwise
 **/
int is_string_length_valid(size_tt length) {
  return length >= 0 && length <= MAXSTRLENG;
}

/**
 * Check if the provided buffer address is a valid one.
 *
 * @param buffer The buffer address
 *
 * @return 1 if it's valid, 0 otherwise
 **/
int is_valid_buffer(char *buffer) {
  return (memaddr) buffer >= KUSEG;
}


// NOTE All the following functions have both the device_id and the asid as parameters. This is not really
// needed, since they are always linked, but it's a measure taken in the spirit of generality advised by the manual.


//-------------------------
//
//         FLASH
//
//-------------------------


/**
 * Read a block from a flash device and copy it to the provided address.
 *
 * @param device_id The flash device number
 * @param asid The ASID assigned to the process requesting the read
 * @param block The block on the flash device that will be read
 * @param out The memory address in which to copy the content of the block
 *
 * @return the code resulting for the read operation
 **/
int read_from_flash_device(int device_id, int asid, int block, memaddr out) {
  // prepare the read command with the target block
  size_tt cmd = FLASHREAD | (block << 8);
  // execute the command and return the return code
  return _interact_with_flash_device(device_id, asid, cmd, out);
}

/**
 * Write a block of a flash device with the data from the provided address.
 *
 * @param device_id The flash device number
 * @param asid The ASID assigned to the process requesting the write
 * @param block The block on the flash device that will be written to
 * @param out The memory address from which to read the data
 *
 * @return the code resulting for the write operation
 **/
int write_to_flash_device(int device_id, int asid, int block, memaddr in) {
  // prepare the write command with the target block
  size_tt cmd = FLASHWRITE | (block << 8);
  // execute the command and return the return code
  return _interact_with_flash_device(device_id, asid, cmd, in);
}

/**
 * Service function used to execute a read or a write operation on a flash device.
 *
 * @param device_id The flash device number
 * @param asid The ASID assigned to the process requesting the operation
 * @param cmd The cmd specifiyng the operation
 * @param out The memory address used by the operation
 *
 * @return the code resulting for the operation
 **/
int _interact_with_flash_device(int device_id, int asid, size_tt cmd, memaddr data0) {

  // recover the device register
  dtpreg_t *reg = (dtpreg_t *)DEV_REG_ADDR(FLASHINT, device_id);

  // acquire lock on the device sem
  support_mutex *mutex = &printer_mutex[device_id];
  support_mutex_lock(asid, mutex);

  // write the out address into the register
  reg->data0 = data0;

  // execute the command
  int return_code = SYSCALL(DOIO, (int)&reg->command, cmd, 0);

  // release lock on the device sem
  support_mutex_release(asid, mutex);

  // return the resulting syscall return code
  return return_code;
}


//-------------------------
//
//        PRINTERS
//
//-------------------------


/**
 * Write a string with the given printer.
 *
 * @param id The printer device number
 * @param asid The ASID assigned to the process requesting the write
 * @param s The pointer to the string
 * @param len How many character to print
 *
 * @return the code resulting for the write operation
 **/
size_tt write_printer(int id, int asid, char *s, size_tt len) {

  // check conditions on the string quality
  if (!is_string_length_valid(len) || !is_valid_buffer(s)) {
    SYSCALL(TERMINATE, 0, 0, 0); 
  }

  // obtain access to the device registers
  dtpreg_t *device = (dtpreg_t *) DEV_REG_ADDR(IL_PRINTER, id);

  int printed = 0;

  // acquire lock on the device sem
  support_mutex *mutex = &printer_mutex[id];
  support_mutex_lock(asid, mutex);

  while (*s != EOS) {

    // prepare the character to write
    device->data0 = *s;
    // prepare the write command
    devregtr value = PRINTCHR;
    // write on the the terminal
    devregtr status = SYSCALL(DOIO, (int)&device->command, (int)value, 0);

    if (status != READY) {
      // the print was NOT successfull

      // release lock on the device sem
      support_mutex_release(asid, mutex);
      // return the number of printed characters until now
      return printed;
    }

    s++;
    printed++;
  }

  // release lock on the device sem
  support_mutex_release_if_held_by_asid(asid, mutex);

  // return the number of printed characters
  return len;
}


//-------------------------
//
//       TERMINALS
//
//-------------------------


/**
 * Write a string on the given terminal.
 *
 * @param id The terminal device number
 * @param asid The ASID assigned to the process requesting the write
 * @param s The pointer to the string
 * @param len How many character to print
 *
 * @return the code resulting for the write operation
 **/
size_tt write_to_terminal(int id, int asid, char *string, size_tt length) {

  // check conditions on the string quality
  if (!is_string_length_valid(length) || !is_valid_buffer(string)) {
    SYSCALL(TERMINATE, 0, 0, 0); 
  }

  // obtain access to the device registers
  termreg_t *device = (termreg_t *) DEV_REG_ADDR(IL_TERMINAL, id);

  int printed = 0;

  // acquire lock on the device sem
  support_mutex *mutex = &terminal_w_mutex[id];
  support_mutex_lock(asid, mutex);

  while (*string != EOS) {
    // prepare the write command
    devregtr value = PRINTCHR | ((devregtr)*string << 8);
    // write on the the terminal
    devregtr status = SYSCALL(DOIO, (int)&device->transm_command, (int)value, 0);

    if ((status & TERMSTATMASK) != RECVD) {
      // the print was NOT successfull

      // release lock on the device sem
      support_mutex_release(asid, mutex);
      // return the number of printed characters until now
      return printed;
    }

    string++;
    printed++;
  }

  // release lock on the device sem
  support_mutex_release_if_held_by_asid(asid, mutex);

  // return the number of printed characters
  return length;

}

/**
 * Read a string from the given terminal.
 *
 * @param id The terminal device number
 * @param asid The ASID assigned to the process requesting the read
 * @param buffer The address of the buffer that will hold the read string
 *
 * @return the code resulting for the read operation
 **/
size_tt read_from_terminal(int id, int asid, char *buffer){ 

  // check the buffer address validity
  if (!is_valid_buffer(buffer)) {
    SYSCALL(TERMINATE, 0, 0, 0); 
  }

  // obtain access to the device registers
  termreg_t *device = (termreg_t *) DEV_REG_ADDR(IL_TERMINAL, id);

  int read = 0;

  // acquire lock on the device sem
  support_mutex *mutex = &terminal_r_mutex[id];
  support_mutex_lock(asid, mutex);

  char s = EOS;
  while (s != NEWLINE) {
    // prepare the read command 
    devregtr status = SYSCALL(DOIO, (int)&device->recv_command, (int)READCHR, 0);

    int status_code = status & TERMSTATMASK;
    if (status_code != TRANSD) {
      // release lock on the device sem
      support_mutex_release(asid, mutex);
      return -status_code;
    }

    // recover the read character
    s = (status >> 8) & TERMSTATMASK;
    *(buffer + read++) = s;
  }

  // release lock on the device sem
  support_mutex_release_if_held_by_asid(asid, mutex);

  // terminate the string, replacing the line feed
  *(buffer + (read - 1)) = EOS;

  // return the read string length (this does not include the EOS)
  return read;
}










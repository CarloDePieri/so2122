#include <umps/libumps.h>

#include <time_manager.h>
#include <myutils.h>
#include <scheduler.h>
#include <main.h>


// Timestamp from the TOD from the last time the current process started (or resumed) execution
unsigned int timestamp_process_job_start;
// Timestamp from the TOD from when the current exception handling started
unsigned int timestamp_exception_start;
// Timestamp of the projected next Interval Timer tick
unsigned int timestamp_next_it_tick;
// Timestamp used when calculating how much time the kernel spent printing debug logs
unsigned int timestamp_logger;
// How much time remains in the current PLT time slice
unsigned int remaining_time_slice;
// How much time the kernel spent logging
unsigned int logger_time;


//------------------------
//     INTERVAL TIMER
//------------------------


/**
 * Init the interval timer and set the timestamp for the projected next tick.
 **/
void init_interval_timer() {
  STCK(timestamp_next_it_tick);
  // The timestamp is used by the update function that will be called after the first tick, so the first needed
  // 'next tick' is actually two ticks from now
  timestamp_next_it_tick += 2*PSECOND;
  LDIT(PSECOND);
}


/**
 * Set the Interval Timer to trigger on the next 100ms tick.
 *
 * NOTE: We use timestamps to make sure not to accumulate delay caused by exceptions handling.
 **/
void set_interval_timer() {
    // calculate how much time is missing from the next projected IT tick
    unsigned int time;
    STCK(time);
    unsigned int time_until_next_it_tick = timestamp_next_it_tick - time;

    // load the interval timer
    LDIT(time_until_next_it_tick);

    // update the next projected interval timer tick
    timestamp_next_it_tick += PSECOND;
}


//-------------------------
//       LOGGER TIME 
//-------------------------


/**
 * Update the logger timestamp.
 **/
void take_logger_timestamp() {
  STCK(timestamp_logger);
}


/**
 * Update the global logger_time.
 *
 * NOTE: we wanted to spare processes the time took by logging in the kernel, so we restricted logger_time updates 
 * to kernel mode processes.
 **/
void update_logger_time() {

  unsigned int log_start_timestamp = timestamp_logger; 
  take_logger_timestamp();

  // Only detract these print time if they are called from kernel mode
  if (((getCAUSE() >> 1) & 0x1) == 0) {
    logger_time += timestamp_logger - log_start_timestamp;
  }
}


/**
 * Time management function to be called at the end of an exception handling before returning control to the current
 * process. It will bill the time taken by the exception handling to the `time_bill_pcb` and, if `time_slice_pcb`
 * priority requires it, resume its PLT time slice.
 *
 * @param time_bill_pcb The pcb that will be billed time
 * @param time_slice_pcb The pcb that could have its time slice resumed
 **/
void TM_bill_before_continue(pcb_t *time_bill_pcb, pcb_t *time_slice_pcb){
  unsigned int exception_time = _calculate_exception_time();
  if (time_bill_pcb != NULL) {
    time_bill_pcb->p_time += exception_time;
  }
  _resume_timeslice_if_needed(time_slice_pcb, exception_time);
  STCK(timestamp_process_job_start);
}


/**
 * Time management function to be called at the end of an exception that's not supposed to bill time to any process.
 * It restarts the PLT if needed and then return the control to the current process.
 *
 * @param saved_state The state saved in the bios data page
 **/
void TM_continue(state_t *saved_state) {
  _resume_timeslice_if_needed(current_process, 0);
  STCK(timestamp_process_job_start);
  LDST(saved_state);
}


/**
 * Like TM_bill_before_continue, but also return control to the current process.
 *
 * @param time_bill_pcb The pcb that will be billed time
 * @param time_slice_pcb The pcb that could have its time slice resumed
 * @param saved_state The state saved in the bios data page
 **/
void TM_bill_and_continue(pcb_t *time_bill_pcb, pcb_t *time_slice_pcb, state_t *saved_state){
  TM_bill_before_continue(time_bill_pcb, time_slice_pcb);
  LDST(saved_state);
}


/**
 * Time management function to be called at the end of an exception handling before calling the scheduler. It will 
 * bill the time taken by the exception handling to the `time_bill_pcb`. It does not interact with the PLT since the
 * scheduler will take care of it later.
 *
 * @param time_bill_pcb The pcb that will be billed time
 **/
void TM_bill_before_schedule(pcb_t *time_bill_target){
  if (time_bill_target != NULL) {
    unsigned int exception_time = _calculate_exception_time();
    time_bill_target->p_time += exception_time;
  }
}


/**
 * Like TM_bill_before_schedule, but also call the scheduler.
 *
 * @param time_bill_pcb The pcb that will be billed time
 * @param saved_state The state saved in the bios data page
 **/
void TM_bill_and_schedule(pcb_t *time_bill_target){
  TM_bill_before_schedule(time_bill_target);
  schedule();
}


/**
 * Time management function to be called at the start of an exception handling. Handle the PLT and bill the current
 * process for the time took in the last execution frame.
 **/
void TM_exception_start(void){

  STCK(timestamp_exception_start);

  if (current_process != NULL) {
    // If a current process exists...

    remaining_time_slice = getTIMER();

    if ((int) remaining_time_slice > 0) {
      // if the PLT is on and still running, defer it 
      setTIMER(0xFFFFFFFF);
    }

    // bill job time to process
    unsigned int job_duration = timestamp_exception_start - timestamp_process_job_start;
    current_process->p_time += job_duration;
  }

  logger_time = 0;
}


/**
 * Time management function to be called before the scheduler cedes control to a low priority process.
 **/
void TM_schedule_start_lp(void) {
    STCK(timestamp_process_job_start);
    _setTIMER(TIMESLICE);
}

/**
 * Time manager function to be called before the scheduler cedes control to a high priority process.
 **/
void TM_schedule_start_hp(void) {
    STCK(timestamp_process_job_start);
}


//-------------------------
//        UTILITIES
//-------------------------


/**
 * Return how much time this exception handling took.
 *
 * NOTE: Since this time will be billed to processes we decided to detract time spent by the kernel logging. We chose
 * this path for two reasons: it seems fair not to assign to processes time spent doing debugging in the kernel; it
 * would also create problems with the low priority timeslice, since our log operations could take several processor
 * cycles away from a process limited timeslice (expecially when converting integers / binaries / addresses to chars).
 *
 * @return the time spent in the last exception
 **/
unsigned int _calculate_exception_time() {
  unsigned int time;
  STCK(time);
  return time - timestamp_exception_start - logger_time;
}


/**
 * Resume the PLT after an exception, if the given `time_slice_pcb`'s priority requires it.
 *
 * @param time_slice_pcb The pcb that could have its time slice resumed
 * @param exception_time How much time the exception handling took
 **/
void _resume_timeslice_if_needed(pcb_t *time_slice_pcb, unsigned int exception_time) {
  if (time_slice_pcb != NULL && time_slice_pcb->p_prio==0 && remaining_time_slice > 0U) {
    unsigned int timeslice = remaining_time_slice - exception_time;
    if ((int) timeslice > 0) {
      // there's still time in this timeslice
      _setTIMER(timeslice);
    }
    else {
      // timeslice ended with this systemcall, make it trigger
      setTIMER(0);
    }

  }
}


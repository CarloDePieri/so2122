#ifndef _DEVICES_MODULE
#define _DEVICES_MODULE

#include "pandos_types.h"


#define EOS             '\0'
#define NEWLINE         '\n'
#define READY           1
#define PRINTCHR        2
#define READCHR         2
#define RECVD           5
#define TRANSD          5
#define TERMSTATMASK    0xFF

typedef unsigned int devregtr;

void init_devices_mutex();
void release_held_devices_mutex(int asid);

int is_string_length_valid(size_tt length);
int is_valid_buffer(char *buffer);

int read_from_flash_device(int device_id, int asid, int block, memaddr out);
int write_to_flash_device(int device_id, int asid, int block, memaddr in);
int _interact_with_flash_device(int device_id, int asid, size_tt cmd, memaddr data0);

size_tt write_printer(int id, int asid, char *s, size_tt len);

size_tt write_to_terminal(int id, int asid, char *string, size_tt length);
size_tt read_from_terminal(int id, int asid, char *buffer);

#endif

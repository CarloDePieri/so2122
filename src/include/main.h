#include <pandos_types.h>

#ifndef _MYMAIN
#define _MYMAIN

#define MAXPROC 20
#define QPAGE 1024

extern int last_pid;
extern int process_count;
extern int soft_block_count;
extern struct list_head lp_ready_queue;
extern struct list_head hp_ready_queue;
extern pcb_t *current_process;
extern int device_semaphores[49];
extern passupvector_t *puv;

#endif

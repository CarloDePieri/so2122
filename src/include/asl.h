#include <pandos_types.h>

extern struct list_head semd_h;

// Default value pointed by a pcb p_semAdd when it's not blocked on a semaphore
#define NOT_A_SEM_KEY 0

semd_t *_allocSemd(int *semAdd);
void _blockPcb(struct pcb_t *p, struct semd_t *sem);
pcb_t *_unblockPcb(pcb_t *p, semd_t * sem);

void initASL();

int insertBlocked(int *semAdd, pcb_t *p);
pcb_t *removeBlocked(int *semAdd);
pcb_t *outBlocked(pcb_t *p);
pcb_t *headBlocked(int *semAdd);

#include <pandos_types.h>


#define FIRST_TERMINAL_REGISTER DEVPERINT * 4

void exception_handler();

/* UTILITIES */
void pass_up_or_die(int index, state_t *saved_state);
int passeren(int *sem_key, pcb_t * pcb, pcb_t **unblocked_pcb);
int verhogen(int *sem_key, pcb_t *pcb, pcb_t **unblocked_pcb);
void kill(pcb_t *pcb);
pcb_t *returnPcbToQueue(pcb_t * pcb);

pcb_t *_scan_pcb_queue(struct list_head *list, int pid);
pcb_t *_find_pcb_by_pid(int pid);

int *findDeviceSemKey(memaddr command_addr);
int *_findDeviceSemKey(int line, int device);
int *_findTerminalSemKey(int device, int mode);

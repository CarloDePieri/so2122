#ifndef _VM_SUPPORT_MODULE
#define _VM_SUPPORT_MODULE

#include "pandos_types.h"


void uTLB_RefillHandler();

// swap pool address, after kernel's .text and .data blocks.
// .data is the last one; at KERNELSTACK + 0x0018 there's the address from which .data begins and at
// KERNELSTACK + 0x0024 there's the size of the .data block
#define SWAP_POOL_ADDR (*(memaddr *)(KERNELSTACK + 0x0018) + *(memaddr *)(KERNELSTACK + 0x0024))

typedef pteEntry_t page_table_t[MAXPAGES];

// pool_page_t is used to model the page size in the struct below
typedef unsigned char pool_page_t[PAGESIZE];

typedef struct swap_pool_t {
  pool_page_t frames[POOLSIZE];  // the frames contains the actual data
  swap_t table[POOLSIZE];  // the table contains info about the swap pool
} swap_pool_t;

void init_swap_pool();

size_tt get_vpn_from_entryhi(size_tt entry_hi);
int get_index_from_vpn(size_tt vpn);
int is_valid_asid(int asid);
int is_frame_occupied(swap_t *frame);
void vm_related_cleanup_on_process_death(int asid);
void init_private_page_table(page_table_t page, int asid);
size_tt get_replacement_frame_index();
void update_tlb_page();
void support_tlb();

#endif

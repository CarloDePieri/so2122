#include <pandos_types.h>

/* PCB LISTS */
void initPcbs();
void mkEmptyProcQ(struct list_head *head);
pcb_t *allocPcb();
void freePcb(pcb_t *p);
int emptyProcQ(struct list_head *head);
void insertProcQ(struct list_head *head, pcb_t *p);
pcb_t *headProcQ(struct list_head *head);
pcb_t *removeProcQ(struct list_head *head);
pcb_t *outProcQ(struct list_head *head, pcb_t *p);

/* PCB TREES */
int emptyChild(pcb_t *p);
void insertChild(pcb_t *prnt, pcb_t *p);
pcb_t *removeChild(pcb_t *p);
pcb_t *outChild(pcb_t *p);


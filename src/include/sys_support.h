#ifndef _INIT_PROC_MODULE
#define _INIT_PROC_MODULE

#include "pandos_types.h"


/* Support level SYS calls */
#define GET_TOD			    1
#define TERMINATE		    2
#define WRITEPRINTER	  3
#define WRITETERMINAL 	4
#define READTERMINAL    5

cpu_t get_tod();
void terminate(support_t *support);
void support_syscall(support_t *support);

void support_program_trap();

void support_generic();

#endif

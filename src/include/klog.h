/*
 * @file klog.h
 * @author Maldus512 
 * @brief Small library that implements a circular log buffer. When properly traced (with ASCII representation),
 *          `klog_buffer` displays a series of printed lines.
 */

#define KLOG_LINES     64     // Number of lines in the buffer. Adjustable, only limited by available memory
#define KLOG_LINE_SIZE 64     // Length of a single line in characters

// Print str to klog
void klog_print(char *str);

// Princ a number in hexadecimal format (best for addresses)
void klog_print_hex(unsigned int num);

void klog_fill_line(unsigned int line, unsigned int starting_char_index, char symbol);

// Move onto the next character (and into the next line if the current one overflows)
void next_char(void);

// Skip to next line
void next_line(void);

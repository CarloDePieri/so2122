#include <pandos_types.h>

#ifndef _INTERRUPT_MANAGER_MODULE
#define _INTERRUPT_MANAGER_MODULE

void interrupt_handler(state_t *saved_state);

#endif

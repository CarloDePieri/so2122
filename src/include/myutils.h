#include <pandos_types.h>
#include <klog.h>
#include <umps3/umps/libumps.h>

#ifndef _MYUTILS
#define _MYUTILS

#define IOFF() setSTATUS(getSTATUS() & ~IECBITON );
#define ION() setSTATUS(getSTATUS() | IECBITON );

/* STRING UTILITIES */
int strcmp(const char *X, const char *Y);
char *strcpy(char *to, char *from);

/* LIST UTILITIES */
int list_len(struct list_head *head);

/* MEMORY MANAGEMENT */
void* memset(void* s, int c, size_tt n);
void memcpy(void* to, void* from, size_tt n);

/* PRINT UTILITIES */
unsigned int mytermprint(char *str, unsigned int term);
unsigned int print0(char *str);
int strlen(char *string);
char *replacePlaceholder(char *text, char *new_text, char placeholder, char *replacement);

char *addrToChar(memaddr p0, char *buffer);
char *replaceAddr(char *string, memaddr ptr, char *target);
int printAddr(char *txt, memaddr ptr, int term);

char *itoa(int value, char *buffer);
char *replaceInt(char *string, int number, char *target);
char *itobin(int a, char *buffer);
char *replaceBin(char *string, int number, char *target);
int printInt(char *string, int number, int term);

/**
 * The following macros mimic function overloading for print and printl.
 **/
#define DEFAULT_TERM 0
// This will mimic type overload: will handle integers and addresses
#define PRINTMULTI(string, any, term) _Generic(any, int: printInt, memaddr: printAddr)(string, any, term)
// These helpers call the right macro/function based on the number of arguments
#define _PRINT1(string) (mytermprint(string, DEFAULT_TERM))
#define _PRINT2(string, any) (PRINTMULTI(string, any, DEFAULT_TERM))
// These will mimic arguments number overload when calling a print with 1 or 2 arguments
#define GET_MACRO(_1,_2,NAME,...) NAME
#define oldprint(...) GET_MACRO(__VA_ARGS__, _PRINT2, _PRINT1)(__VA_ARGS__)
#define oldprintl(...) GET_MACRO(__VA_ARGS__, _PRINT2, _PRINT1)(__VA_ARGS__); mytermprint("\n", DEFAULT_TERM);

/* LOG UTILITIES */

void logAddr(char *string, memaddr addr);
void logInt(char *string, int number);
// function overloading for log and logl
#define LOGMULTI(string, any) _Generic(any, int: logInt, memaddr: logAddr)(string, any)
#define _LOG1(string) (klog_print(string))
#define _LOG2(string, any) (LOGMULTI(string, any))
#define log(...) GET_MACRO(__VA_ARGS__, _LOG2, _LOG1)(__VA_ARGS__)
#define logl(...) GET_MACRO(__VA_ARGS__, _LOG2, _LOG1)(__VA_ARGS__); klog_print("\n");

void _log_start(char *logger);
void _log_end();
#define _log_v2(logger, message) _log_header(logger); log(message);
#define _log_v3(logger, message, any) _log_header(logger); log(message, any);
#define _LOGH2(logger, string) _log_v2(logger, string)
#define _LOGH3(logger, string, any) _log_v3(logger, string, any)
#define GET_MACRO2(_1,_2,_3,NAME,...) NAME
// #define hlog(...) _log_start(logger); log(__VA_ARGS__); _log_end();
// #define hlogl(...) _log_start(logger); logl(__VA_ARGS__); _log_end();
// TODO DOcument this flag
#define COMPILE_WITH_DEBUG_PRINT TRUE
// hlog and hlogl are used like this:
// char *logger = "logger_name";
// hlog("log this n: %i", n);
#define hlog(...) \
  ({ \
   if (COMPILE_WITH_DEBUG_PRINT) { \
   _log_start(logger); log(__VA_ARGS__); _log_end(); \
   } \
   })
#define hlogl(...) \
  ({ \
   if (COMPILE_WITH_DEBUG_PRINT) { \
   _log_start(logger); logl(__VA_ARGS__); _log_end(); \
   } \
   })

// TODO Is this lecit?
#define xlogl(...) IOFF(); hlogl(__VA_ARGS__); ION();
#define bplogl(...) IOFF(); hlogl(__VA_ARGS__); bp2(); ION();


/* DEBUGGING UTILITIES */
void bp();
void bp1();
void bp2();
void assertSameAddr(void *a, void *b);

/**
 * The TRACE macro can be used to COPY an object in an area of the memory that the umps3 debugger can then
 * easly inspect.
 *
 *
 **/
#define STRINGIFY(a) #a
#define TRACE(obj, type, name) \
    { \
      _Pragma(STRINGIFY(GCC diagnostic ignored "-Wunused-but-set-variable")) \
      static type name; \
      name = obj; \
    }

//    hex struct dimension
// --------------------------
//  pcb_t                 AC

#endif

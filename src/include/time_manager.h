#include <pandos_types.h>

#ifndef _TIME_MANAGER_MODULE
#define _TIME_MANAGER_MODULE

extern unsigned int timestamp_exception_start;
extern unsigned int timestamp_process_job_start;
extern unsigned int timestamp_next_it_tick;
extern unsigned int timestamp_logger;
extern unsigned int remaining_time_slice;
extern unsigned int logger_time;

void init_interval_timer();
void set_interval_timer();

void update_logger_time();
void take_logger_timestamp();

void TM_schedule_start_lp(void);
void TM_schedule_start_hp(void);

void TM_exception_start(void);

void TM_bill_before_continue(pcb_t *time_bill_pcb, pcb_t *time_slice_pcb);
void TM_bill_and_continue(pcb_t *time_bill_pcb, pcb_t *time_slice_pcb, state_t *saved_state);

void TM_continue(state_t *saved_state);

void TM_bill_before_schedule(pcb_t *time_bill_target);
void TM_bill_and_schedule(pcb_t *time_bill_target);

unsigned int _calculate_exception_time();
int _update_p_time(void);
void _resume_timeslice_if_needed(pcb_t *time_slice_pcb, unsigned int exception_time);

// This adjust the timer with the time scale
#define _setTIMER(timer) setTIMER(timer * (*((cpu_t *) TIMESCALEADDR)));

#endif

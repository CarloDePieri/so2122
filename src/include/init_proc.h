#ifndef _SYS_SUPPORT_MODULE
#define _SYS_SUPPORT_MODULE

#include "pandos_types.h"


void masterSemaphore_V ();
void masterSemaphore_P ();

// A structure to pair a mutex to an asid
typedef struct support_mutex {
  int sem;            // the semaphore
  int asid;           // the asid of the process holding the sem, -1 othersise
} support_mutex;

void support_mutex_lock(int asid, support_mutex *sem);
void support_mutex_release(int asid, support_mutex *sem);
void support_mutex_release_if_held_by_asid(int asid, support_mutex *sem);


void init_support_structures();
support_t *allocate();
void deallocate(support_t *support);

void test();

#endif

#include <pandos_types.h>

#ifndef _SYSCALL_MANAGER_MODULE
#define _SYSCALL_MANAGER_MODULE

void syscall_handler(state_t *saved_state);

#endif

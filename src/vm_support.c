#include "listx.h"
#include "pandos_const.h"
#include <myutils.h>
#include <umps3/umps/libumps.h>
#include "pandos_types.h"
#include "umps/const.h"
#include "umps/types.h"
#include <vm_support.h>
#include <main.h>
#include <umps/arch.h>
#include <sys_support.h>
#include <devices.h>
#include <init_proc.h>


//---------------------------------------------------------------------------------
//
//    TLB REFILL HANDLER
//
//    NOTE This is called directly by the Nucleus, so it has access to the same
//    global structures, like current_process.
//
//---------------------------------------------------------------------------------


/**
 * Handle TLB-Refill events.
 * Take the missing page from the process support structure and load it into the TLB before returning
 * control to the process.
 **/
void uTLB_RefillHandler() {

  state_t *saved_state = (state_t *)BIOSDATAPAGE;

  // get the vpn from the EntryHI
  size_tt vpn = get_vpn_from_entryhi(saved_state->entry_hi);

  // calculate the page table index 
  int index = get_index_from_vpn(vpn);

  // recover the page from the process page table
  pteEntry_t page = current_process->p_supportStruct->sup_privatePgTbl[index];

  // write the page into the TLB
  setENTRYHI(page.pte_entryHI);
  setENTRYLO(page.pte_entryLO);
  TLBWR();

  // return control to the process
  LDST(saved_state);
}


//---------------------------------------------------------------------------------------------------------
//
//    SWAP POOL
//
//    NOTE For ease of access both the Swap Pool and the Swap Pool Table are handled by the swap_pool_t
//    struct below.
//
//---------------------------------------------------------------------------------------------------------

swap_pool_t *swap_pool;

// mutex for the swap pool
static support_mutex swap_pool_mutex;

/**
 * Init all structures needed to handle the Swap Pool.
 **/
void init_swap_pool() {

  char *logger = "pger";

  // set the swap pool address
  swap_pool = (swap_pool_t *) SWAP_POOL_ADDR;

  hlogl("Swap pool table init");

  // init the swap pool table
  for (int i = 0; i < POOLSIZE; ++i) {
    swap_pool->table[i].sw_asid = -1;
  }

  swap_pool_mutex.asid = -1;
  swap_pool_mutex.sem = 1;

}


//-----------------------------
//
//  VIRTUAL MEMORY MANAGEMENT
//
//-----------------------------


/**
 * Check if the provided asid is in the range 1..UPROCMAX.
 *
 * @param asid The asid to check for validity
 *
 * @return 1 if the asid is valid, 0 otherwise
 **/
int is_valid_asid(int asid) {
  return asid >= 1 && asid <= UPROCMAX;
}

/**
 * Check if the provided frame has already been assigned to a process.
 *
 * @param frame The frame to check
 *
 * @return 1 if the frame is already occupied, 0 otherwise
 **/
int is_frame_occupied(swap_t *frame) {
  return frame->sw_asid != -1;
}

/**
 * Return the Virtual Page Number from an EntryHI register.
 *
 * @param entry_hi The EntryHI register
 *
 * @return The VPN as an unsigned int
 **/
size_tt get_vpn_from_entryhi(size_tt entry_hi) {
  return entry_hi >> VPNSHIFT;
}

/**
 * Return the index of the given VPN in the process private page table. It will be a number in the range 0..MAXPAGES,
 * with the last page reserved to the stack.
 *
 * @param vpn The Virtual Page Number
 *
 * @return The index in the page table
 **/
int get_index_from_vpn(size_tt vpn) {
  if (vpn == (USERSTACKTOP >> VPNSHIFT) - 1) {
    // it's the stack, return the last page index
    return MAXPAGES - 1;
  }
  memaddr base = KUSEG >> VPNSHIFT;
  if (vpn < base || vpn >= base + (MAXPAGES - 1)) {
    // a valid vpn must be in KUSEG, in the first 31 page (the last one is for the stack)
    char *logger = "pger";
    hlog("Invalid vpn request: %p", vpn);
    PANIC();
  }
  return vpn - base;
}

/**
 * Perform operations needed for killing cleanly a process: make sure the lock on the Swap Pool Table is released 
 * if held and mark all occupied frame in the Swap Pool as unoccupied.
 *
 * @param asid The process ASID
 **/
void vm_related_cleanup_on_process_death(int asid) {

  // release the swap pool lock if held
  support_mutex_release_if_held_by_asid(asid, &swap_pool_mutex);

  // mark its frame as unoccupied
  for (int i = 0; i < POOLSIZE; i++) {
    if (swap_pool->table[i].sw_asid == asid) {
      swap_pool->table[i].sw_asid = -1;
    }
  }
}

/**
 * Prepare a process private page table. It will mark as read-only pages dedicated to the .text block.
 *
 * @param page_table The private page table
 * @param asid The asid of the process owning the table
 **/
void init_private_page_table(page_table_t page_table, int asid) {

  char *logger = "pger";

  // read the very first block from the backing store and save it in memory
  pool_page_t first_block;
  int read_status = read_from_flash_device(asid-1, asid, 0, (memaddr) &first_block);
  if (read_status != 1) {
    hlogl("Could not read the first block for ASID %i", asid);
    PANIC();
  }
  // at +0x0014 there's the .text file size, which are 20 bytes; from there calculate the last page used by .text
  int last_text_page_index = first_block[20] / PAGESIZE;

  if (page_table == NULL || !is_valid_asid(asid)) {
    hlogl("Can't init ASID %i private page", asid);
    PANIC();
  }
  int stack_page = MAXPAGES - 1;
  int shifted_asid = asid << ASIDSHIFT;

  for (int i = 0; i < stack_page; i++) {
    page_table[i].pte_entryHI = KUSEG + (i << VPNSHIFT) + shifted_asid;
    // mark pages used by .text as read-only
    page_table[i].pte_entryLO = i <= last_text_page_index ? 0 : DIRTYON;
  }
  page_table[stack_page].pte_entryHI = ((USERSTACKTOP - 1) & ~0x000FFF) + shifted_asid;
  page_table[stack_page].pte_entryLO = DIRTYON;

}

/**
 * Implements PandOS Page Replacement Algorithm. It will look for a free frame and fallback to a FIFO policy.
 *
 * @return The swap pool frame index
 **/
size_tt get_replacement_frame_index() {
  // pick the first free frame
  for (size_tt i = 0; i < POOLSIZE; i++) {
    if (swap_pool->table[i].sw_asid == -1) {
      return i;
    }
  }
  // otherwise use fifo
  static size_tt frame_index = -1;
  frame_index = (frame_index + 1) % POOLSIZE;
  return frame_index;
}

/**
 * Look for the given page in the TLB and update its entry if found.
 *
 * @param page The page to update
 **/
void update_tlb_page(pteEntry_t *page) {
  // look for the page
  setENTRYHI(page->pte_entryHI);
  TLBP();
  
  if (!(getINDEX() & PRESENTFLAG)) {
    // page found, update it!
    setENTRYHI(page->pte_entryHI);
    setENTRYLO(page->pte_entryLO);
    TLBWI();
  }

};

/**
 * The pager function handling all support level TLB exception (TLB-mod, TLBL and TLBS).
 **/
void support_tlb() {

  char *logger = "pger";

  support_t *support = (support_t *)SYSCALL(GETSUPPORTPTR, 0, 0, 0);
  state_t *state = &support->sup_exceptState[PGFAULTEXCEPT];

  if (state->cause == 1) {
    // TLB-mod
    support_program_trap();
  }
  else {
    // TLBL or TLBS

    xlogl("Page fault");

    int asid = support->sup_asid;
    // id of the flash device used as backing store
    int device_id = asid - 1;

    // acquire swap pool table mutex
    support_mutex_lock(asid, &swap_pool_mutex);

    size_tt vpn = get_vpn_from_entryhi(state->entry_hi);
    int block_index = get_index_from_vpn(vpn);

    // chose a frame from the swap pool
    size_tt frame_index = get_replacement_frame_index();
    memaddr swap_frame_addr = (memaddr) swap_pool->frames[frame_index];
    swap_t *swap_frame_info = &swap_pool->table[frame_index];

    if ( is_frame_occupied(swap_frame_info) ) {

      // if the chosen frame was already occupied, we need to make sure its information are preserved

      size_tt occupied_vpn = get_vpn_from_entryhi(swap_frame_info->sw_pte->pte_entryHI);
      int occupied_block_index = get_index_from_vpn(occupied_vpn);
      
      IOFF();

        // Instructions in this section must be done atomically, hence the interrupts turned off

        // mark process page as invalid
        swap_frame_info->sw_pte->pte_entryLO &= ~VALIDON;

        // update the TLB if needed
        update_tlb_page(swap_frame_info->sw_pte);

      ION();

      // update the process backing store (flash device)
      int write_status = write_to_flash_device(device_id, asid, occupied_block_index, swap_frame_addr);
      if (write_status != 1) {
        support_program_trap();
      }

    }

    // Read from the process backing store and save the block into the swap pool
    int read_status = read_from_flash_device(device_id, asid, block_index, swap_frame_addr);
    if (read_status != 1) {
      support_program_trap();
    }

    IOFF();

      // Instructions in this section must be done atomically, hence the interrupts turned off

      // update the swap pool frame
      swap_frame_info->sw_asid = asid;
      swap_frame_info->sw_pageNo = vpn;
      swap_frame_info->sw_pte = &support->sup_privatePgTbl[block_index];

      // update the process page
      support->sup_privatePgTbl[block_index].pte_entryLO =  swap_frame_addr | DIRTYON | VALIDON;

      // update the TLB if needed
      update_tlb_page(&support->sup_privatePgTbl[block_index]);

    ION();

    // release swap pool table mutex
    support_mutex_release_if_held_by_asid(asid, &swap_pool_mutex);

    // return control to the process
    LDST(state);

  }

}

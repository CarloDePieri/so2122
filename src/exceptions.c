#include "pandos_types.h"
#include <umps/cp0.h>
#include <umps/libumps.h>
#include <umps/arch.h>

#include <exceptions.h>
#include <time_manager.h>
#include <myutils.h>
#include <asl.h>
#include <pcb.h>
#include <syscall_manager.h>
#include <interrupt_manager.h>
#include <scheduler.h>
#include <main.h>


/**
 * Handle exceptions based on the exception code.
 **/
void exception_handler(){

  TM_exception_start();

  char *logger = "excp";

  state_t *saved_state = (state_t *) BIOSDATAPAGE;
  int exc_code = CAUSE_GET_EXCCODE(saved_state->cause);

  hlogl( "Code %i", exc_code);

  switch ( exc_code )
  {
    case 0:
      // interrupt
      interrupt_handler(saved_state);
      break;
    case 1:
    case 2:
    case 3:
      // TLB exceptions
      {{
         hlogl("TLB");
         pass_up_or_die(PGFAULTEXCEPT, saved_state);
       }}
      break;
    case 4:
    case 5:
    case 6:
    case 7:
    case 9:
    case 10:
    case 11:
    case 12:
      // Program traps
      {{
         pass_up_or_die(GENERALEXCEPT, saved_state);
       }}
      break;
    case 8:
      // Syscall
      syscall_handler(saved_state);
      break;
  }
};


//-------------------------
//        UTILITIES
//-------------------------


/**
 * Perform a standard pass-up-or-die operation, delegating the the user's exception handler, if defined, killing the
 * process otherwise.
 *
 * @param index Either GENERALEXCEPT or PGFAULTEXCEPT
 * @param saved_state The state before the exception, saved in the bios data page
 **/
void pass_up_or_die(int index, state_t *saved_state) {

  if (current_process->p_supportStruct != NULL) {
    // PASS UP

    // save to the process passup vector the current exception state
    current_process->p_supportStruct->sup_exceptState[index] = *saved_state;

    // recover the process passup vector context
    context_t context = current_process->p_supportStruct->sup_exceptContext[index];

    // Handle time from here
    TM_bill_before_continue(current_process, current_process);
    
    // load the context
    LDCXT(context.stackPtr, context.status, context.pc);
  }
  else {
    // DIE
    kill(current_process);

    schedule();
  }
}


/**
 * A Passeren operation a binary semaphore. Sometimes access to the unblocked pcb is needed, hence why the pointer to
 * the pointer of a pcb in the arguments. This function is specular to the verhogen function.
 *
 * @param sem_key The address of the semaphore
 * @param pcb The pcb of the process that called the P
 * @param unblocked_pcb A pointer to the pointer to the unblocked pcb
 *
 * @return 0, if the calling process is blocked on the semaphore; 1, if the operation unblocked a pcb; 2, if the
 * semaphore value got decremented
 **/
int passeren(int *sem_key, pcb_t * pcb, pcb_t **unblocked_pcb){

  int return_code;

  if (*sem_key == 0) {
    insertBlocked(sem_key, pcb);
    return_code = 0;
  }
  else if (headBlocked(sem_key) != NULL){
    // *sem_key = 1 and there's a pcb blocked on the sem
    *unblocked_pcb = returnPcbToQueue(removeBlocked(sem_key));
    return_code = 1;
  }
  else {
    (*sem_key)--;
    return_code = 2;
  }

  return return_code;
}


/**
 * A Verhogen operation a binary semaphore. Sometimes access to the unblocked pcb is needed, hence why the pointer to
 * the pointer of a pcb in the arguments. This function is specular to the passeren function.
 *
 * @param sem_key The address of the semaphore
 * @param pcb The pcb of the process that called the P
 * @param unblocked_pcb A pointer to the pointer to the unblocked pcb
 *
 * @return 0, if the calling process is blocked on the semaphore; 1, if the operation unblocked a pcb; 2, if the
 * semaphore value got decremented
 **/
int verhogen(int *sem_key, pcb_t *pcb, pcb_t **unblocked_pcb) {

  int return_code;


  if (*sem_key == 1) {
    insertBlocked(sem_key, pcb);
    return_code = 0;
  }
  else if (headBlocked(sem_key) != NULL){
    // *sem_key = 0 and there's a pcb blocked on the sem
    pcb_t *tmp = removeBlocked(sem_key);
    *unblocked_pcb = returnPcbToQueue(tmp);
    return_code = 1;
  }
  else {
    (*sem_key)++;
    return_code = 2;
  }

  return return_code;
}


/**
 * Kill the process represented by the given `pcb`. Recursively kill all his children too.
 * If `pcb` is NULL it does nothing.
 *
 * @param pcb The target pcb that will be killed
 **/
void kill(pcb_t *pcb) {

  if (pcb != NULL) {
    struct list_head* iter;
    list_for_each(iter,&pcb->p_child) {
      pcb_t *child = container_of(iter, pcb_t, p_sib);
      kill(child);
    }

    // orphan this child
    outChild(pcb);

    if (current_process != NULL && current_process->p_pid == pcb->p_pid) {
      // It's the current process
      current_process = NULL;
    }
    else if (pcb->p_semAdd != NOT_A_SEM_KEY) {
      // it's blocked on a semaphores
      if (pcb->p_semAdd >= device_semaphores && pcb->p_semAdd <= device_semaphores + 49 * sizeof(int)  ) {
        // if it's blocked on a system device semaphore, adjust soft_block_count
        soft_block_count--;
      }

      // remove it from its semaphore. do not modify the semaphore value, the P/V will take care of this
      outBlocked(pcb);
    }
    else {
      // it's on a ready queue, remove it from the queue
      if (pcb->p_prio == 0) {
        outProcQ(&lp_ready_queue, pcb);
      }
      else
        outProcQ(&hp_ready_queue, pcb);
    }

    // return the pcb to the free pcb list
    freePcb(pcb);

    process_count--;
  }
}


/**
 * Return a `pcb` to its ready queue, depending on its priority.
 *
 * @param pcb The pcb that will be returned to its ready queue
 **/
pcb_t *returnPcbToQueue(pcb_t * pcb) {
  if (pcb != NULL) {
    if (pcb->p_prio == 0) {
      insertProcQ(&lp_ready_queue, pcb);
    }
    else {
      insertProcQ(&hp_ready_queue, pcb);
    }
  }
  return pcb;
}


/**
 * Look for a pcb by `pid` in a pcb list.
 *
 * @param list The list_head of the list that will be scanned
 * @param pid The process id
 *
 * @return the found pcb, NULL otherwise
 **/
pcb_t *_scan_pcb_queue(struct list_head *list, int pid) {
  struct list_head* iter;
  list_for_each(iter,list) {
    pcb_t *pcb = container_of(iter, pcb_t, p_list);
    if (pcb->p_pid == pid) {
      return pcb;
    }
  }
  return NULL;
}


/**
 * Find the pcb with the given `pid`.
 *
 * @param pid The process id
 *
 * @return the found pcb, NULL otherwise
 **/
pcb_t *_find_pcb_by_pid(int pid) {
  pcb_t * result = NULL;

  // check the current_process
  if (current_process->p_pid == pid) {
    return current_process;
  }

  // check the lp_ready_queue
  result = _scan_pcb_queue(&lp_ready_queue, pid);
  if (result != NULL)
    return result;

  // check the hp_ready_queue
  result = _scan_pcb_queue(&hp_ready_queue, pid);
  if (result != NULL)
    return result;

  // check every active semaphore
  struct list_head* iter_sem;
  list_for_each(iter_sem,&semd_h) {
    semd_t * sem = container_of(iter_sem, semd_t, s_link);
    // check its blocked process queue
    result = _scan_pcb_queue(&sem->s_procq, pid);
    if (result != NULL)
      return result;
  }

  return result;
}


/**
 * Find the correct semaphore from the global device_semaphores array using the device `command_addr`.
 *
 * @param command_addr The address of the command register of the device
 *
 * @return the corresponding semaphores key
 **/
int *findDeviceSemKey(memaddr command_addr) {

  // all registers have the same size, so I can find out the register position
  int register_index =  (command_addr - DEV_REG_START) / DEV_REG_SIZE;

  if (register_index < FIRST_TERMINAL_REGISTER) {
    // it's a non-terminal device
    int line_index = register_index / N_DEV_PER_IL;
    int lines[4] = {IL_DISK,IL_FLASH,IL_ETHERNET,IL_PRINTER}; 
    int line = lines[line_index];
    int devNo = register_index % N_DEV_PER_IL;

    return _findDeviceSemKey(line, devNo);
  }
  else {
    // it's a terminal device
    int devNo = register_index - FIRST_TERMINAL_REGISTER;
    memaddr devAddrBase = DEV_REG_ADDR(IL_TERMINAL, devNo);
    int mode = command_addr == (devAddrBase + 0x4) ? 0 : 1;

    return _findTerminalSemKey(devNo, mode);
  }
}


/**
 * Return the semaphores key associated with a non-terminal device.
 *
 * @param line The device line
 * @param device The device number
 *
 * @return the corresponding semaphores key
 **/
int *_findDeviceSemKey(int line, int device) {
  int key_index = (line - 3) * 8 + device;
  return &device_semaphores[key_index];
}


/**
 * Return the semaphores key associated with a terminal device.
 *
 * @param device The terminal device number
 * @param mode 0 if recv, 1 if transm
 *
 * @return the corresponding semaphores key
 **/
int *_findTerminalSemKey(int device, int mode) {
  int key_index = (7 - 3) * 8 + device + mode;
  return &device_semaphores[key_index];
}

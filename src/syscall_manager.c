#include <umps/cp0.h>
#include <umps/libumps.h>

#include <syscall_manager.h>
#include <myutils.h>
#include <pcb.h>
#include <asl.h>
#include <exceptions.h>
#include <scheduler.h>
#include <time_manager.h>
#include <main.h>


/**
 * Handles the syscall exceptions.
 *
 * @param saved_state The state before the exception, saved in the bios data page
 **/
void syscall_handler(state_t *saved_state) {

  char *logger = "sysc";

  int processor_mode = (saved_state->status & KUPBITON ) >> 3;
  unsigned int a0 = saved_state->reg_a0;
  unsigned int a1 = saved_state->reg_a1;
  unsigned int a2 = saved_state->reg_a2;
  unsigned int a3 = saved_state->reg_a3;

  if (processor_mode == 0 && (int)a0 < 0) {
    // kernel mode syscall

    hlogl("KERNEL mode: %i", (int) a0);

    switch (a0)
    {
      case CREATEPROCESS:
        // -1: create a new process
        {{
           pcb_t *new = allocPcb();

           if (new != NULL) {
             // set the new process state
             new->p_s = *(state_t *) a1;

             // make sure the priority is set correctly
             if (a2 == 1) {
               new->p_prio = 1;
             }
             else {
               new->p_prio = 0;
             }

             // set the optional support structure
             if (a3 != (memaddr) NULL) {
               new->p_supportStruct = (support_t *) a3;
             }

             // enquee the process in the correct queue
             returnPcbToQueue(new);

             // add the new process as a child
             insertChild(current_process, new);

             new->p_time = 0;

             process_count++;

             // return the new process id
             saved_state->reg_v0 = new->p_pid;
           }
           else {
             // No free pcb
             saved_state->reg_v0 = -1;
           }

           saved_state->pc_epc = saved_state->pc_epc + WORDLEN;

           TM_bill_and_continue(current_process, current_process, saved_state);
         }}
        break;
      case TERMPROCESS:
        // -2: KILLLLLLLLL
        {{
           if (a1 == 0) {
             // suicide
             kill(current_process);
           }
           else {
             // murder
             pcb_t * to_kill = _find_pcb_by_pid(a1);

             // Save the pcb and the state in case this process does not get killed
             saved_state->pc_epc += WORDLEN;
             current_process->p_s = *saved_state;

             if (to_kill != NULL) {
               // a process with the given pid was found!
               saved_state->reg_v0 = 0;
               kill(to_kill);
             }
             else {
               saved_state->reg_v0 = 1;
             }

             if (current_process != NULL) {
               // return control to the current process if it's alive
               TM_bill_and_continue(current_process, current_process, saved_state);
             }
           }
           // the current process was killed, return control to the scheduler
           schedule();
         }}
        break;
      case PASSEREN:
        // -3: P operation on a semaphore
        {{
           int *sem_key = (int *) a1;

           pcb_t *unblocked = NULL; // unused here
           int p_rc = passeren(sem_key, current_process, &unblocked);

           saved_state->pc_epc += WORDLEN;
           if (p_rc == 0) {
             current_process->p_s = *saved_state;
             current_process = NULL;
             TM_bill_and_schedule(current_process);
           }
           else {
             TM_bill_and_continue(current_process, current_process, saved_state);
           }
         }}
        break;
      case VERHOGEN:
        // -4: V operation on a semaphore
        {{
           int *sem_key = (int *) a1;

           pcb_t *unblocked = NULL; // unused here
           int v_rc = verhogen(sem_key, current_process, &unblocked);

           saved_state->pc_epc += WORDLEN;
           if (v_rc == 0) {
             current_process->p_s = *saved_state;
             current_process = NULL;
             TM_bill_and_schedule(current_process);
           }
           else {
             TM_bill_and_continue(current_process, current_process, saved_state);
           }
         }}
        break;
      case DOIO:
        // -5: DO Input Output operation on a device
        {{
           current_process->p_s = *saved_state;
           current_process->p_s.pc_epc += WORDLEN;

           // Write the command value to the command addr
           memaddr *ptr = (memaddr *) a1;
           *ptr = a2;

           // find out the correct semaphore
           int *semKey = findDeviceSemKey(a1);
           // being these binary semaphores, it will always block
           pcb_t *unblocked = NULL; // unused here
           int p_rc = passeren(semKey, current_process, &unblocked);
           if (p_rc == 0) {
             current_process = NULL;
           }

           soft_block_count++;

           TM_bill_and_schedule(current_process);
         }}
        break;
      case GETTIME:
        // -6: Get process p_time
        {{
           // current_process->p_time already contains its current time slice spent fragment, since it has been 
           // updated at the start of the exceptions handler function
           saved_state->reg_v0 = current_process->p_time;
           saved_state->pc_epc += WORDLEN;
           TM_bill_and_continue(current_process, current_process, saved_state);
         }}
        break;
      case CLOCKWAIT:
        // -7: wait for the IT
        {{
           pcb_t *unblocked = NULL; // unused here
           passeren(&device_semaphores[48], current_process, &unblocked); // it will block for sure

           soft_block_count++;

           saved_state->pc_epc += WORDLEN;
           current_process->p_s = *saved_state;

           TM_bill_before_schedule(current_process);

           current_process = NULL;

           schedule();
         }}
        break;
      case GETSUPPORTPTR:
        // -8: get support data pointer
        {{
           saved_state->reg_v0 = (memaddr) current_process->p_supportStruct;
           saved_state->pc_epc += WORDLEN;
           TM_bill_and_continue(current_process, current_process, saved_state);
         }}
        break;
      case GETPROCESSID:
        // -9: Get process id
        {{
           if (a1 == 0) {
             // self pid requested
             saved_state->reg_v0 = current_process->p_pid;
           }
           else {
             // parent pid requested
             if (current_process->p_parent == 0) {
               // this is the root process
               saved_state->reg_v0 = 0;
             }
             else {
               saved_state->reg_v0 = current_process->p_parent->p_pid;
             }
           }
           saved_state->pc_epc += WORDLEN;
           TM_bill_and_continue(current_process, current_process, saved_state);
         }}
        break;
      case YIELD:
        // -10: yield control of the processor
        {{
           saved_state->pc_epc += WORDLEN;
           current_process->p_s = *saved_state;

           returnPcbToQueue(current_process);

           TM_bill_before_schedule(current_process);

           current_process = NULL;

           schedule();
         }}
        break;
      default:
        // non existing system call
        {{
           // set the ExcCode to RI
           saved_state->cause = (saved_state->cause & ~CAUSE_EXCCODE_MASK) | (EXC_RI << CAUSE_EXCCODE_BIT);
           pass_up_or_die(GENERALEXCEPT, saved_state);
         }}
    }

  }
  else if (processor_mode == 1 && (int)a0 < 0) {
    // unauthorized!
    hlogl("USER mode SYSTEM syscall: %i", (int) a0);
    // set the ExcCode to RI
    saved_state->cause = (saved_state->cause & ~CAUSE_EXCCODE_MASK) | (EXC_RI << CAUSE_EXCCODE_BIT);
    pass_up_or_die(GENERALEXCEPT, saved_state);
  }
  else {
    // syscall code >= 0
    hlog("MODE %i", processor_mode);
    hlogl("HIGH syscall: %i", (int) a0);

    pass_up_or_die(GENERALEXCEPT, saved_state);
  }

}

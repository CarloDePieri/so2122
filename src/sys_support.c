#include "listx.h"
#include "pandos_const.h"
#include "pandos_types.h"
#include "umps/types.h"
#include <myutils.h>
#include <umps/cp0.h>
#include <umps/arch.h>
#include <sys_support.h>
#include <vm_support.h>
#include <devices.h>
#include <init_proc.h>


//----------------------------
//
//    SUPPORT SYSTEM CALLS
//
//----------------------------


/**
 * Return how much time passed since the system was turned on.
 *
 * @return The time in microseconds
 **/
cpu_t get_tod() { 
  cpu_t time;
  STCK(time);
  return time;
}

/**
 * Terminate the current support level process cleanly.
 *
 * @param The support structure of the current process
 **/
void terminate(support_t *support) {

  // release every device lock held
  release_held_devices_mutex(support->sup_asid);

  // virtual memory related cleanups
  vm_related_cleanup_on_process_death(support->sup_asid);

  // release the support structure
  deallocate(support);

  // tell init_proc we are done here
  masterSemaphore_V();

  // kill the process
  SYSCALL(TERMPROCESS, 0, 0, 0);
}

/**
 * The user system call manager. It will put the result of the syscall in the v0 register.
 *
 * @param The support structure of the process calling the syscall
 **/
void support_syscall(support_t *support) {

  state_t *state = &support->sup_exceptState[GENERALEXCEPT];
  int syscall_code = state->reg_a0;

  char *logger = "usys";
  xlogl("USER mode: %i", syscall_code);

  int asid = support->sup_asid;
  int device_id = asid - 1;
  int result;

  switch (syscall_code) {
    case GET_TOD:
        result = get_tod();
        break;
    case WRITEPRINTER:
      {{
        char *s = (char *) state->reg_a1;
        size_tt len = state->reg_a2;

        result = write_printer(device_id, asid, s, len);

        break;
       }}
    case WRITETERMINAL:
      {{
        char *string = (char *) state->reg_a1;
        size_tt length = state->reg_a2;

        result = write_to_terminal(device_id, asid, string, length);

        break;
       }}
    case READTERMINAL:
      {{
        char *buffer = (char *) state->reg_a1;

        result = read_from_terminal(device_id, asid, buffer);

        break;
       }}
    case TERMINATE:
    default:
        terminate(support);
        return;
  }
  state->reg_v0 = result;
}


//---------------------------------
//
//    SUPPORT EXCEPTION HANDLER
//
//---------------------------------


/**
 * The support exception handler.
 **/
void support_generic() {

  char *logger = "ugen";

  support_t *support = (support_t *)SYSCALL(GETSUPPORTPTR, 0, 0, 0);
  state_t *state = &support->sup_exceptState[GENERALEXCEPT];
  int code = CAUSE_GET_EXCCODE(state->cause);

  xlogl("Code %i", code);

  switch (code) {
    case 8:
      support_syscall(support);
      break;
    default:
      support_program_trap();
  }

  // advance the pc
  state->pc_epc += WORDLEN;
  state->reg_t9 += WORDLEN;

  // return control to the process
  LDST(state);

}


//------------------------------------
//
//    SUPPORT PROGRAM TRAP HANDLER
//
//------------------------------------


/**
 * The support program trap handler.
 **/
void support_program_trap() {
  SYSCALL(TERMINATE, 0, 0, 0); 
}

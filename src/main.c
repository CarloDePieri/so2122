/* #include <pandos_const.h> */
/* #include <pandos_types.h> */
/* #include <umps3/umps/libumps.h> */

#include <myutils.h>
#include <pcb.h>
#include <asl.h>
#include <exceptions.h>
#include <scheduler.h>
#include <main.h>
#include <time_manager.h>


// These are taken from the initProc file
extern int test();
extern int uTLB_RefillHandler();

// last used process id
int last_pid;

// running process count
int process_count;

// process blocked on devices or timers that we know will return
int soft_block_count;

// ready process queues
struct list_head lp_ready_queue;
struct list_head hp_ready_queue;

// currently running process
pcb_t *current_process;

// all devices' semaphore id + interval timer semaphore id
//
// 0-7   Disks devices      (IL 3)
// 8-15  Flash devices      (IL 4)
// 16-23 Network devices    (IL 5)
// 24-31 Printer devices    (IL 6)
// 32-47 Terminal devices   (IL 7)  (recv, transm)
// 48 pseudo-clock
int device_semaphores[49];

// processor 0 pass up vector
passupvector_t *puv = (passupvector_t *)PASSUPVECTOR;


int main(void) {

  // Init pass up vector
  puv->tlb_refill_handler = (memaddr) uTLB_RefillHandler;
  puv->tlb_refill_stackPtr = KERNELSTACK;
  puv->exception_handler = (memaddr) exception_handler;
  puv->exception_stackPtr = KERNELSTACK;

  // Init data structs
  initPcbs();
  initASL();

  // Init Nucleus vars
  last_pid = 0;
  process_count = 0;
  soft_block_count = 0;
  mkEmptyProcQ(&lp_ready_queue);
  mkEmptyProcQ(&hp_ready_queue);
  current_process = NULL;
  memset(device_semaphores, 0, sizeof(device_semaphores));

  // load the interval timer
  init_interval_timer();

  pcb_t *first = allocPcb();
  // Interrupt on, kernel-mode and local processor timer on
  first->p_s.status = first->p_s.status | IEPBITON | TEBITON | IMON;

  // SP to RAMTOP
  memaddr ramtop;
  RAMTOP(ramtop);
  first->p_s.reg_sp = ramtop;

  // PC to test function address
  first->p_s.pc_epc = first->p_s.reg_t9 = (memaddr) test;

  // add the process the lp_ready_queue
  insertProcQ(&lp_ready_queue, first);
  process_count++;

  schedule();

  return 0;
}

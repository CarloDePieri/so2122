# Cross toolchain variables
# If these are not in your path, you can make them absolute.
XT_PRG_PREFIX = mipsel-linux-gnu-
CC = $(XT_PRG_PREFIX)gcc
LD = $(XT_PRG_PREFIX)ld
SHELL = /bin/bash

# uMPS3-related paths

# Simplistic search for the umps3 installation prefix.
# If you have umps3 installed on some weird location, set UMPS3_DIR_PREFIX by hand.
ifneq ($(wildcard /usr/bin/umps3),)
	UMPS3_DIR_PREFIX = /usr
else
	UMPS3_DIR_PREFIX = /usr/local
endif

UMPS3_DATA_DIR = $(UMPS3_DIR_PREFIX)/share/umps3
UMPS3_INCLUDE_DIR = $(UMPS3_DIR_PREFIX)/include
UMPS3_INCLUDE_DIR2 = $(UMPS3_DIR_PREFIX)/include/umps3

# Compiler options
CFLAGS_LANG = -ffreestanding
CFLAGS_MIPS = -mips1 -mabi=32 -mno-gpopt -EL -G 0 -mno-abicalls -fno-pic -mfp32
CFLAGS = $(CFLAGS_LANG) $(CFLAGS_MIPS) -I$(UMPS3_INCLUDE_DIR) -I$(UMPS3_INCLUDE_DIR2) -Isrc/include -Wall -O0

# Linker options
LDFLAGS = -G 0 -nostdlib -T $(UMPS3_DATA_DIR)/umpscore.ldscript -m elf32ltsmip

# Add the location of crt*.S to the search path
VPATH = $(UMPS3_DATA_DIR)

# Project dirs
SRC_DIR = src
LIBS_DIR = $(SRC_DIR)/libs
INCLUDE_DIR = $(SRC_DIR)/include
TESTS_DIR = $(SRC_DIR)/tests
TESTER_DIR = testers

GROUP_NAME=lso22az02
GIT_REPO=https://gitlab.com/CarloDePieri/so2122.git

.PHONY : all clean main watch_main docs tar dev run testers clean_testers clean_outputs

all : testers main

###
# main kernel
###
main : kernel.core.umps

kernel.core.umps : kernel
	umps3-elf2umps -k $<

kernel : time_manager.o pcb.o asl.o myutils.o crtso.o libumps.o klog.o init_proc.o devices.o vm_support.o sys_support.o exceptions.o scheduler.o interrupt_manager.o syscall_manager.o main.o 
	$(LD) -o $@ $^ $(LDFLAGS)

main.o : $(SRC_DIR)/main.c
	$(CC) $(CFLAGS) -c -o $@ $<

syscall_manager.o : $(SRC_DIR)/syscall_manager.c
	$(CC) $(CFLAGS) -c -o $@ $<

interrupt_manager.o : $(SRC_DIR)/interrupt_manager.c
	$(CC) $(CFLAGS) -c -o $@ $<

time_manager.o : $(SRC_DIR)/time_manager.c
	$(CC) $(CFLAGS) -c -o $@ $<

asl.o : $(LIBS_DIR)/asl.c
	$(CC) $(CFLAGS) -c -o $@ $<

pcb.o : $(LIBS_DIR)/pcb.c
	$(CC) $(CFLAGS) -c -o $@ $<

klog.o : $(LIBS_DIR)/klog.c
	$(CC) $(CFLAGS) -c -o $@ $<

devices.o : $(SRC_DIR)/devices.c
	$(CC) $(CFLAGS) -c -o $@ $<

sys_support.o : $(SRC_DIR)/sys_support.c
	$(CC) $(CFLAGS) -c -o $@ $<

vm_support.o : $(SRC_DIR)/vm_support.c
	$(CC) $(CFLAGS) -c -o $@ $<

init_proc.o : $(SRC_DIR)/init_proc.c
	$(CC) $(CFLAGS) -c -o $@ $<

exceptions.o : $(SRC_DIR)/exceptions.c
	$(CC) $(CFLAGS) -c -o $@ $<

scheduler.o : $(SRC_DIR)/scheduler.c
	$(CC) $(CFLAGS) -c -o $@ $<

myutils.o : $(LIBS_DIR)/myutils.c $(INCLUDE_DIR)/myutils.h
	$(CC) $(CFLAGS) -c -o $@ $<

# Pattern rule for assembly modules
%.o : %.S
	$(CC) $(CFLAGS) -c -o $@ $<

###
# Testers-specific targets
###
testers :
	cd $(TESTER_DIR) && $(MAKE)

clean_testers :
	cd $(TESTER_DIR) && $(MAKE) clean

###
# misc
###
clean : clean_testers clean_outputs
	rm -f *.o kernel kernel.*.umps

clean_outputs :
	rm -f term*.umps printer*.umps

# create compile_commands.json used by clangd LS using bear
compile_commands.json : Makefile
	bear -- make main

watch_main:
	inotifywait --exclude '\.t|\.o|\.umps' -r -e moved_to,create,close_write -m $(SRC_DIR) $(TESTER_DIR) | \
	while read -r directory events filename; do \
		$(MAKE) --no-print-directory main testers; \
	done

run:
	umps3 umps3.json

dev:
	make watch_main &
	make run

docs:
	doxygen doxygen.conf

# compile a tar.gz with the HEAD of the repo, stripped of the .git folder
tar:
	git clone $(GIT_REPO) $(GROUP_NAME) && \
		export REL_NAME="$(GROUP_NAME)_$$(cd $(GROUP_NAME); git rev-parse --short HEAD)" && \
		rm -rf $(GROUP_NAME)/.git && \
		mv $(GROUP_NAME) $$REL_NAME && \
		tar caf $$REL_NAME.tar.gz $$REL_NAME && \
		rm -rf $$REL_NAME && \
		sha1sum $$REL_NAME.tar.gz

# same as tar, but allows to modify the folder before compriming it
tar-split:
	git clone $(GIT_REPO) $(GROUP_NAME) && \
		export REL_NAME="$(GROUP_NAME)_$$(cd $(GROUP_NAME); git rev-parse --short HEAD)" && \
		echo -e "\nWhen you are done, type:\n\nenv REL_NAME=$$REL_NAME make _tar-end\n" && \
		rm -rf $(GROUP_NAME)/.git && \
		mv $(GROUP_NAME) $$REL_NAME

_tar-end:
		tar caf $$REL_NAME.tar.gz $$REL_NAME && \
		rm -rf $$REL_NAME && \
		sha1sum $$REL_NAME.tar.gz
